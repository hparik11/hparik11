Name- Harsh Parikh       CWID- A20338453



CREATE TABLE RecCentreMember (
ID number NOT NULL,
PRIMARY KEY (ID),
F_name Varchar(20),
L_name Varchar(20),
Dob DATE,
Family_ID INT ,
FOREIGN KEY (Family_ID) REFERENCES FamilyPackage(ID)
 --Family_ID INT references FamilyPackage.ID

);

CREATE TABLE FamilyPackage (
ID INT NOT NULL,
PRIMARY KEY (ID),
Address Varchar(100),
Phone VARCHAR UNIQUE

);

create table Class_Type(

Type varchar(20) not null,
PRIMARY Key(Type),
Description varchar(100)
);

CREATE table Instructor(
ID INT NOT NULL,
PRIMARY KEY (ID),
F_name Varchar(20),
L_name Varchar(20),
Member_ID number,
FOREIGN KEY (Member_ID) REFERENCES RecCentreMember(ID)
);


create table class(
ID INT NOT NULL,
PRIMARY KEY (ID),
Title varchar(20) ,
Type varchar(20),
FOREIGN KEY (Type) REFERENCES Class_Type(Type),
Instructor number,
FOREIGN KEY (Instructor) REFERENCES Instructor(ID),
Season varchar(20),
Year int
);

Create table Enrollment(
Class_Id int,
FOREIGN KEY (Class_Id) REFERENCES class(ID),
Member_ID number,
FOREIGN KEY (Member_ID) REFERENCES RecCentreMember(ID),
cost int
);