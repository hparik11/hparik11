Name- Harsh Parikh       CWID- A20338453





Insert into class(ID,Title,Type,Instructor,Season,Year) values (1,'Needle points', 'Craft', 2,'Spring', 2010);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (2, 'Photography', 'Art', 1,'Fall' ,2008);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (3, 'Woodworking', 'Craft', 4, 'Spring', 2009);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (4, 'Chinese (Intro.)', 'Languages', 1, 'Winter', 2008);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (5, 'Team games', 'Kids', 1, 'Summer', 2008);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (6, 'Yoga (Intro.)',' Exercise', 2, 'Fall',2009);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (7, 'Origami (Adv.)', 'Craft', 4, 'Fall', 2009);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (8, 'Oil painting', 'Art', 3,'Spring', 2009);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (9, 'Yoga (Adv.)', 'Exercise', 1, 'Spring', 2008);
Insert into class(ID,Title,Type,Instructor,Season,Year) values (10, 'Chinese (Intro.)', 'Languages' ,3, 'Spring' ,2009);





INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(1,'Abby','Smith','05/21/1983',1);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(2,'Mike','O''Shea','07/04/1968',2);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(3,'April','O''Shea','06/23/1954',2);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(4,'Vijay','Gupta','08/01/1945',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(5,'Lisa','Tang','11/05/2000',3);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(6,'Harry','Smith','02/03/1972',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(7,'Justin','Smith','02/02/1983',1);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(8,'Lisa','Brown','12/28/1959',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(9,'Harry','Tang','04/03/1948',3);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(10,'Dongmei','Tang','06/02/1942',3);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(11,'Laura','Dickinson','11/11/1998',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(12,'Victor','Garcia','04-05-2006',5);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(13,'Emily','Citrin','05-04-1993',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(14,'Maria','Garcia','07-07-2007',5);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(15,'Cassie','O''Shea','06-02-1988',2);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(16,'Cassandra','McDonald','07-01-1990',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(17,'Jessie','Knapp','09-12-1981',4);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(18,'Monica','Knapp','09-17-1982',4);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(19,'Leslie','Blackburn','01-19-1986',null);
INSERT INTO RecCentreMember(ID,F_Name,L_Name,dob,Family_ID) values(20,'Sandra','Svoboda','09-09-1999',null);

commit;

INSERT INTO  FamilyPackage(ID,Address, Phone) values(1,'23 Beacon St. Hillside IL','708-555-9384');
INSERT INTO  FamilyPackage(ID,Address, Phone) values(2,'4930 Dickens Ave Chicago IL','312-555-9403');
INSERT INTO  FamilyPackage(ID,Address, Phone) values(3,'345 Fullerton St. Chicago IL','773-555-0032');
INSERT INTO  FamilyPackage(ID,Address, Phone) values(4,'34 Maple Ln Elmhurst IL','312-555-9382');
INSERT INTO  FamilyPackage(ID,Address, Phone) values(5,'563 Harvard Ave Lisle IL','630-555-9321');

commit;


Insert into Class_Type(Type,Description) values ('Craft','Knitting, sewing, ect');
Insert into Class_Type(Type,Description) values('Art','Paining, sculpting, ect');
Insert into Class_Type(Type,Description) values('Exercise','Any courses having to do with physical activity');
Insert into Class_Type(Type,Description) values('Languages','Anything to do with writing, literature, or communication');
Insert into Class_Type(Type,Description) values('Kids','Courses geared towards children 13 and younger');



Insert into Instructor(ID,F_Name,L_Name,Member_ID) values(1, 'Annie','Heard',null);
Insert into Instructor(ID,F_Name,L_Name,Member_ID) values(2,'Monica','Knapp',18);
Insert into Instructor(ID,F_Name,L_Name,Member_ID) values(3, 'James', 'Robertson',null);
Insert into Instructor(ID,F_Name,L_Name,Member_ID) values(4, 'April', 'O''Shea', 2);
Insert into Instructor(ID,F_Name,L_Name,Member_ID) values(5, 'Harry', 'Tang', 9);



insert into Enrollment(Class_Id,Member_ID,cost) values(3, 3, 20);
insert into Enrollment(Class_Id,Member_ID,cost) values(1, 9, 15);
insert into Enrollment(Class_Id,Member_ID,cost) values(2, 9, 20);
insert into Enrollment(Class_Id,Member_ID,cost) values(4 ,10 ,30);
insert into Enrollment(Class_Id,Member_ID,cost) values(3 ,10 ,10);
insert into Enrollment(Class_Id,Member_ID,cost) values(5, 5, 10);
insert into Enrollment(Class_Id,Member_ID,cost) values(4, 9, 30);
insert into Enrollment(Class_Id,Member_ID,cost) values(1, 11, 25);
insert into Enrollment(Class_Id,Member_ID,cost) values(2, 19, 40);
insert into Enrollment(Class_Id,Member_ID,cost) values(7, 14, 10);
insert into Enrollment(Class_Id,Member_ID,cost) values(8, 12, 5);
insert into Enrollment(Class_Id,Member_ID,cost) values(1, 1, 30);
insert into Enrollment(Class_Id,Member_ID,cost) values(6, 1, 15);
insert into Enrollment(Class_Id,Member_ID,cost) values(9, 1, 20);
insert into Enrollment(Class_Id,Member_ID,cost) values(8, 1, 25);
insert into Enrollment(Class_Id,Member_ID,cost) values(1, 13, 18);
insert into Enrollment(Class_Id,Member_ID,cost) values(2, 20, 9);
insert into Enrollment(Class_Id,Member_ID,cost) values(10, 4, 15);
insert into Enrollment(Class_Id,Member_ID,cost) values(1, 2, 3);
INSERT INTO ENROLLMENT(Class_Id,Member_ID,cost) VALUES(5, 1, 16);
INSERT INTO ENROLLMENT(Class_Id,Member_ID,cost) VALUES(5, 7, 20);
INSERT INTO ENROLLMENT(Class_Id,Member_ID,cost) VALUES(4, 12,13);
INSERT INTO ENROLLMENT(Class_Id,Member_ID,cost) VALUES(5, 14, 30);
INSERT INTO ENROLLMENT(Class_Id,Member_ID,cost) VALUES(5, 12,35);


