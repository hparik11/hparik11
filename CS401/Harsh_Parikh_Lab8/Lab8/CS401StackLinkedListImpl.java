package Lab8;

public class CS401StackLinkedListImpl<E> implements CS401StackInterface<E> 
{
	   private LinkEntry<E> head;
	   private int num_elements;

	   public CS401StackLinkedListImpl()
	   {
	      head = null;
	      num_elements = 0;
	   }
	 
	   public void push(CS401BinaryTree.TreeNode<E> n)
	   {
	      /** Add code here **/
		   LinkEntry<E> ne = new LinkEntry<E>();
		   ne.element =(E) n;
		   ne.next=head;
		   head=ne;
		   num_elements=num_elements+1;
		 
	      return;
	   }

	   public E pop()
	   {
		
		   
	      /** Add code here **/
		   E e = null;
		   
		   if(head==null)
		   {
			   return e;
		   }
		   else
		   {
			   E temp = head.element;
			   head = head.next;
			   num_elements=num_elements-1;
			
			   return temp;
		   }
		  
		   
	   }

	   public int size()
	   {
	      /** Add code here **/
		   return num_elements;
	   }
	   public boolean isEmpty()
	   {
		   if(head == null)
			   return true;
		   else
			   return false;
	   }

	   /* ------------------------------------------------------------------- */
	   /* Inner classes                                                      */
	   protected class LinkEntry<E>
	   {
	      protected E element;
	      protected LinkEntry<E> next;

	      protected LinkEntry()
	      { 
	    	  element = null; 
	    	  next = null; 
	      }
	   }
	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		
	}

	
} /* CS401StackLinkedListImpl<E> */
