import java.util.LinkedList;
import java.util.Scanner;

public class Evaluator extends CS401StackLinkedListImpl{
public static void main(String args[])
{
	//String infix="8-2+8/4+6-1-6/2";
	
	
	System.out.print("Enter Infix without space: ");
	Scanner sc = new Scanner (System.in);
	String infix=sc.nextLine(); 
	String postfix="";
	System.out.println("Infix : "+infix);
	
	CS401StackLinkedListImpl<Character> Stack = new CS401StackLinkedListImpl<Character>();
	int i=infix.length();
	//System.out.println("Size= " +i);
	int l=0;
	LinkedList<Character> operators = new LinkedList<Character>();   //Operators initialization. 
	operators.add('/');
	operators.add('*');
	operators.add('+');
	operators.add('-');
	operators.add('(');
	operators.add(')');
	
	//int a;
	
	int j = 1;
	
	while(l<i)
	{
		
		char a = infix.charAt(l);
		
		if (operators.contains(a))                   //If  any operators are coming
		{
			if(Stack.size() == 0)                    //Check the size of Stack==0
			{
				Stack.push(a);
			}
			else                                     //If Stack-Size is not Zero
			{
				char top=Stack.pop();
				if(check(a) > check(top))            //Checking the precedence of operators
				{
					
					Stack.push(top);
					Stack.push(a);
					
				}//If
			
				else if (check(a) <= check(top))
				{
					
					if(Stack.size()>=1)
					{
						char b=Stack.pop();
						Stack.push(a);
						
						postfix=postfix+top+b;
					} 
					else
					{
						Stack.push(a);
						
						postfix=postfix+top;
					}
				}//Else_IF
				
				else
				{
					if(a=='(')
					{
						while(a!=')')
						{
							//Stack.push(top);
							char b=Stack.pop();
							l++;
										
						}
						
					}//IF
					
				}//Else
		
			}//Else
							
		}//IF
		
		
		else                                      //If any operand(Numerical Value) comes
		{
			postfix=postfix+a;
			
		}//Else
		l++;
	
	}	//While 
	
	while(Stack.size()>0)                                      //Printing the rest of elements
	{
		
			postfix=postfix+ Stack.pop();
	    
	}
	System.out.println("Postfix : "+ postfix);

	
	
CS401StackLinkedListImpl<String> outputStack = new CS401StackLinkedListImpl<String>();
	int l1 = postfix.length();
	int i1 = 0;
	while(i1<l1)
	{
		char a1 = postfix.charAt(i1);
		if(operators.contains(a1))                                           //Checking for operators 
		{

			String o1 = outputStack.pop().toString();                
			String o2 = outputStack.pop().toString();
			float temp = 0;
			if(a1=='+'){                                                     // Doing operations which are called
				temp = Float.parseFloat(o2)+Float.parseFloat(o1);
			}else if(a1=='-'){
				temp = Float.parseFloat(o2)-Float.parseFloat(o1);
			}else if(a1=='*'){
				temp = Float.parseFloat(o2)*Float.parseFloat(o1);
			}else if(a1=='/'){
				temp = Float.parseFloat(o2)/Float.parseFloat(o1);
			}
			
			String temp1 = String.valueOf(temp);
			outputStack.push(temp1);
		}
		else 																//This loop is for operands 
		{
			String a2 = String.valueOf(a1);
			outputStack.push(a2);											//It will directly push it on stack.
			
		}
		i1++;
	}
	System.out.println("ANS : "+ outputStack.pop());
} 



public static int check(char a)                                              //Check precedence of operators.
{
	
    if(a=='/' || a=='*')
		return 2;
	else if(a=='+' || a=='-')
		return 1;
return 0;
}

}


