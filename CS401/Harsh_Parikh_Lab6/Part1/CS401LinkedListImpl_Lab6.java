import java.util.Iterator;


public class CS401LinkedListImpl_Lab6<E> implements CS401CollectionInterface<E>,
                                               Iterable<E>
{
   private LinkEntry<E> head;
   private LinkEntry<E> tail;
   private int s = 1;

   public CS401LinkedListImpl_Lab6()
   {
      head = tail = null;
   }

   public boolean is_empty()
   {
      if (head == null) 
          return true;

      return false;
   }

   public boolean is_full() { return false; }

   public int size()
   {
      return size_r(head) - 1;
      /*
       * Note that an iterative solution would be as follows:
       * 
       * LinkEntry<E> temp;
       * int i = 0;
       *
       * for (temp = head; temp != null; temp = temp.next)
       *      i++;
       * return i;
       */
   }

   public boolean add(int index, E e)
   {
      throw new UnsupportedOperationException();
   }

   public boolean add(E e)
   {
      LinkEntry<E> ne = new LinkEntry<E>();
      ne.element = e;

      if (head == null && tail == null)
      {
          head = tail = ne;
          
      }
      else
      {
         tail.next = ne;
         tail = ne;
        
      }
            
      return true;
      
   }//add method

   public boolean add_sorted(E e)
   {
      LinkEntry<E> ne = new LinkEntry<E>();
      ne.element = e;

      if (head == null && tail == null)
      {
          head = tail = ne;
      }
      else
      {
         LinkEntry<E> prev = null; 
         LinkEntry<E> temp;

         for (temp = head; temp != null; temp = temp.next)
         {
            int comp = ((Comparable)e).compareTo(temp.element);
            if (comp < 0) /* Element added is less than one on list. */
            {
                break;
            }
            prev = temp;
         }

         if (prev == null)  /* Adding as new head */
         {
             ne.next = head;
             head = ne;
         }
         else if (temp == null)  /* Adding as new tail */
         {
             tail.next = ne;
             tail = ne;
         }
         else  /* Adding in the middle */
         {
             ne.next = prev.next;   /* HAS TO BE IN THIS ORDER */
             prev.next = ne;        /* HAS TO BE IN THIS ORDER */
         }
      }

      return true;
   }

   public E remove(int index)
   {
       throw new IndexOutOfBoundsException();
   }

   public E remove()
   {
      throw new UnsupportedOperationException();
   }

   public E get(int index)
   {
      throw new UnsupportedOperationException();
   }

   public boolean contains(E e)
   {
      throw new UnsupportedOperationException();
   }

   public Iterator<E> iterator()  /* From Interface Iterable */
   {
      return new CS401LinkedListIterator(); 
   }

   private int size_r(LinkEntry<E> head)  /* Think about this recursive call! */
   {
      if (head != null)
         s = s + size_r(head.next);
      return s;
   }

   /* ------------------------------------------------------------------- */
   /* Inner classes                                                      */
   protected class LinkEntry<E>
   {
      protected E element;
      protected LinkEntry<E> next;
   }
   /* ------------------------------------------------------------------- */
   protected class CS401LinkedListIterator implements Iterator<E>
   {
      protected LinkEntry<E> current;
      
      protected LinkEntry<E> prev1;
      
      protected LinkEntry<E> prev2;
      
      
      protected CS401LinkedListIterator() { 
    	  current = head;
    	  prev1 = null;
    	  prev2 = null;
      }

      public E next() 
      {
    	  
    	  E e = current.element;
    	  prev2 = prev1;
    	  prev1 = current;
    	  if (current != tail)  {
             current = current.next;
    	  } else {
    		  current = null; 
    	  }
         
    	  return e;
      }//Next

      public boolean hasNext() { 
    	  return current != null; 
     
      }//HasNext

      /*
       * In Java Iterators, the remove() method removes from the underlying
       * collection the last element returned by the iterator (see
       * http://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html).
       *
       * Implement the remove() method below with the expectation that it
       * behaves in the same way as described above.
       *
       * The expectation is that the following code should work as 
       * expected:
       * 
       *   Iterator iter = CS401LinedkedListImpl.iterator();
       *   while (iter.hasNext())  {
       *      E elem = iter.next();
       *      if (elem matches what I am looking for)  {
       *         iter.remove();
       *      }
       *   }
       */
     
      public void remove()
      {
         /** Add code here **/ 
    	  
    	  if(prev1==null){
    		  throw new IllegalStateException();
    	  }
    	  if(prev2==null)
    	  {
    		  head=current;
    	  }
    	  else
    	  {
    		  prev2.next=current;
    		  prev1=prev2;
    		  
    	  }     
    	  
       }//Remove Method

   }//Iterator Method
   
   
   public static void main(String args[]) 
   {
	   		
		
//----------------------------------------Method A------------------------------------------	   
	   
	   CS401LinkedListImpl_Lab6<Chores> obj = new CS401LinkedListImpl_Lab6<Chores>();         
		Chores a = new Chores("Make Bed", 10);
		
		obj.add(a);
		
		Iterator<Chores> itr=obj.iterator();
		System.out.println(" ------Method A: Delete alone element------ ");
		while(itr.hasNext())
		{
		System.out.println(itr.next());
		}
		itr.remove();
		
		System.out.println("After removing There are "+obj.size()+ " elements in this list");
		
		
		
//----------------------------------------Method B------------------------------------------
		
		CS401LinkedListImpl_Lab6<Chores> obj1 = new CS401LinkedListImpl_Lab6<Chores>();    
		
	//	Chores a = new Chores("Make Bed", 10);
		Chores b = new Chores("Do Laundry", 5);
		
		obj1.add(a);
		obj1.add(b);
		Iterator<Chores> itr1=obj1.iterator();
		System.out.println("\n ------Method B: Remove the first element.------");
		int i=0;
		while(itr1.hasNext())
		{
			System.out.print(itr1.next()+" ");
			i++;
		}
		System.out.println();
		itr1=obj1.iterator();
		itr1.next();
		itr1.remove();
		//itr1.next();
		System.out.println("After removing There are "+obj1.size()+ " elements in this list");
		//itr1=obj1.iterator();
		while(itr1.hasNext())
		System.out.println(itr1.next()+" --> Null");
		
		
//------------------------------------Method C-------------------------------------------		
		
		
		CS401LinkedListImpl_Lab6<Chores> obj2 = new CS401LinkedListImpl_Lab6<Chores>();    
		
		//	Chores a = new Chores("Make Bed", 10);
		//	Chores b = new Chores("Do Laundry", 5);
			
			obj2.add(a);
			obj2.add(b);
			Iterator<Chores> itr2=obj2.iterator();
			System.out.println("\n ------Method C: Remove the second element.------");
			int j=0;
			while(itr2.hasNext())
			{
				System.out.print(itr2.next()+" ");
				j++;
			}
			System.out.println();
			itr2=obj2.iterator();
			for(int n=0;n<j;n++)
			{
				itr2.next();
			}
			itr2.remove();
			
			System.out.println("After removing there are "+obj2.size()+ " elements in this list");
			itr2=obj2.iterator();
			while(itr2.hasNext())
			System.out.print(itr2.next()+" ");
			System.out.println("--> Null");
			
//------------------------------------Method D------------------------------------------------			
			
			CS401LinkedListImpl_Lab6<Chores> obj3 = new CS401LinkedListImpl_Lab6<Chores>();    
			
			    Chores c = new Chores("Take out garbage", 20);

				obj3.add(a);
				obj3.add(b);
				obj3.add(c);
				Iterator<Chores> itr3=obj3.iterator();
				System.out.println("\n ------Method D: Remove the middle element.------");
				int k=0;
				while(itr3.hasNext())
				{
					System.out.print(itr3.next()+" ");
					k++;
				}
				System.out.println();
				itr3=obj3.iterator();
				for(int n=0;n<k-1;n++)
				{
					itr3.next();
				}
				itr3.remove();
				
				System.out.println("After removing there are "+obj3.size()+ " elements in this list");
				itr3=obj3.iterator();
				while(itr3.hasNext())
				System.out.print(itr3.next()+" "); 
				System.out.println("-->Null");

		
	 }//main

} /* CS401LinkedListImpl_Lab6<E> */

