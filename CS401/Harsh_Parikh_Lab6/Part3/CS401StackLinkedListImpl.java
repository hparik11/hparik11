

public class CS401StackLinkedListImpl<E> implements CS401StackInterface<E> 
{
   private LinkEntry<E> head;
   private int num_elements;

   public CS401StackLinkedListImpl()
   {
      head = null;
      num_elements = 0;
   }

   public void push(E e)                                  //Here we point to current element and
   {
      /** Add code here **/								  // add next element at head. and 
	   LinkEntry<E> ne = new LinkEntry<E>();
	   ne.element = e;									  // point this at head.
	   ne.next=head;
	   head=ne;
	   num_elements=num_elements+1;
      //return;
   }//Push Method

   public E pop()
   {														//In popping, we put first value into temporary variable and
      /** Add code here **/
	   E n = null;											//then just pointing head to next value. and 
	   if(head==null)		
	   {													//decrement size.
		   return n;
	   }
	   else
	   {
		   E temp = head.element;
		   head=head.next;
		   num_elements=num_elements-1;
		   return temp;
	   }
	  
	   
   }//Pop Method

   public int size()
   {
      /** Add code here **/
	   return num_elements;
   }

   /* ------------------------------------------------------------------- */
   /* Inner classes 
    * 
    *                                                      
    *                                                      */
   
   public boolean is_empty()
   {
      if (head == null) 
          return true;

      return false;
   }
   protected class LinkEntry<E>
   {
      protected E element;
      protected LinkEntry<E> next;

      protected LinkEntry() { element = null; next = null; }
   }
   
   
	public static void main(String[] args)
	{
			
		CS401StackLinkedListImpl<String> mystack = new CS401StackLinkedListImpl<String>();
		
		System.out.println("Initially stack size " + mystack.size());
		mystack.push("my");
		mystack.push("name");
		mystack.push("is");
		mystack.push("Harsh");
	
		System.out.println("After data Entry "+mystack.size()+"\n");
		System.out.println("My Data Entry in reverse order ");
		String a = mystack.pop();
		System.out.println(a);
		String b  = mystack.pop();
		System.out.println(b);
		b  = mystack.pop();
		System.out.println(b);
		b  = mystack.pop();
		System.out.println(b);
		
	}
	

} /* CS401StackLinkedListImpl<E> */
