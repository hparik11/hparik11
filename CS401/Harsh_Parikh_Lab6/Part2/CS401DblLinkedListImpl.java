import java.util.Iterator;
import java.util.Scanner;

public class CS401DblLinkedListImpl<E> implements CS401CollectionInterface<E> 

{
   private LinkEntry<E> head;        
   private LinkEntry<E> tail;        
   private int s = 1;

   public CS401DblLinkedListImpl()
   {
      head = tail = null;
   }

   public boolean is_empty()
   {
      if (head == null) 
          return true;
      else
      return false;
   }

   public boolean is_full() { return false; }

   public int size()
   {
      return size_r(head) - 1;
      /*
       * Note that an iterative solution that traverses the list from
       * the front would be as follows:
       * 
       * LinkEntry<E> temp;
       * int i = 0;
       *
       * for (temp = head; temp != null; temp = temp.next)
       *      i++;
       * return i;
       */
   }

   public boolean add(int index, E e)
   {
      throw new UnsupportedOperationException();
   }

   /*
    * Add e to the end of the doubly linked list.
    * Returns true - if e was successfully added, false otherwise.
    */
   public boolean add(E e)
   {
      /** Add code here **/   
	   
	   LinkEntry<E> ne = new LinkEntry<E>();
	   ne.element = e;
	   
	   if (head == null && tail == null)
	      {
	          head = tail = ne;
	         
	      }
	      else
	      {
	          tail.next=ne;
	    	  ne.previous=tail;
	    	  tail=ne;
	    	  // ne.next=tail;
	    	  //tail.previous=ne;
	      }
	            
	      return true;
      
   }//Add method

   /*
    * Remove the nth element in the list.  The first element is element 1.
    * Return the removed element to the caller.
    */
   public E remove(int n)
   {
      /** Add code here **/ 
	   if (head == null && tail == null)
	      {
		   return null;
	      }
	   if (n < 1 || n > size())
	   {
		   return null;
	   }
	   LinkEntry<E> ne = new LinkEntry<E>();
	   //ne.element = e;
	   int i=1;
	   ne=head;
	   while(i<n)
	   {
		   ne=ne.next;
		   i++;
	   }
	   
       if(ne==head)
        {
        	head=head.next;
        	head.next.previous=head;
        	
        }
        else if(ne==tail)
        {
        	ne.previous.next=null;
        	tail=ne.previous;
        }
        else
        {
        	ne.previous.next=ne.next;
        	ne.next.previous=ne.previous;
        }
      return null;
      
   }//Remove Method

   /*
    * Print the doubly linked list starting at the beginning.
    */
   public void print_from_beginning()
   {
      /** Add code here **/
	   LinkEntry<E> b = new LinkEntry<E>();
	   b=head;
	   while(b!=null)
	   {
		   System.out.println(b.element);
		   b=b.next;
	   }
	}//Print_From_Beginning Method

   /*
    * Print the doubly linked list starting the end.
    */
   public void print_from_end()
   {
      /** Add code here **/
	   LinkEntry<E> e = new LinkEntry<E>();
	   e=tail;
	   while(e!=null)
	   {
		   
		   System.out.println(e.element);
		   e=e.previous;
		   
	   }
     
   }//Print_From_End Method

   public E remove()
   {
      throw new UnsupportedOperationException();
   }

   public E get(int index)
   {
      throw new UnsupportedOperationException();
   }

   public boolean contains(E e)
   {
      throw new UnsupportedOperationException();
   }
   public Iterator<E> iterator()  /* From Interface Iterable */
   {
      return new CS401DblLinkedListIterator(); 
   }

   private int size_r(LinkEntry<E> head)  /* Think about this recursive call! */
   {
      if (head != null)
         s = s + size_r(head.next);
      return s;
   }
   /* ------------------------------------------------------------------- */
   /* Inner classes                                                      */
   protected class LinkEntry<E>
   {
      protected E element;
      protected LinkEntry<E> next;
      protected LinkEntry<E> previous;

      protected LinkEntry() { element = null; next = previous = null; }
   }//LinkEntry
   
   protected class CS401DblLinkedListIterator implements Iterator<E>
   {
      protected LinkEntry<E> current;
      
      protected CS401DblLinkedListIterator() { 
    	  current = head;
    	  
      }

      public E next() 
      {
    	  
    	  E e = current.element;
    	 
    	  if (current != null)  {
             current = current.next;
    	  } 
         
    	  return e;
      }//Next

      public boolean hasNext() { 
    	  return current != null; 
     
      }//HasNext

  }//Iterator
   
   public static void main(String args[]) 
   {
	   		
		CS401DblLinkedListImpl<String> obj=new CS401DblLinkedListImpl<String>();
		
		obj.add("Bill");
		obj.add("Rohan");
		obj.add("James");
		obj.add("Krishna");
		obj.add("Javier");
		obj.add("Lisa");
		
		System.out.println("Enter Choice: 1 / 2 / 3 / 4 / 5 ");
		int i;
		Scanner scan = new Scanner (System.in);
		i = scan.nextInt();
		
		switch(i)
		{
		case 1:
			System.out.println("----Print from Beginning---- \n");
			obj.print_from_beginning();
			System.out.println();
			break;
			
		case 2:
			System.out.println("----Print from end---- \n");
			obj.print_from_end();
			System.out.println();
			break;
		case 3:
			System.out.println("Remove Bill and print the linked list starting from beginning. \n");
			obj.remove(1);
			obj.print_from_beginning();
			System.out.println();
			
			break;
		case 4:
			System.out.println("Remove Lisa and print the linked list starting from end. \n");
			obj.remove(6);
			obj.print_from_end();
			System.out.println();
			break;
		case 5:
			System.out.println("Remove Krishna and print the linked list starting from the beginning. \n");
			obj.remove(4);
			obj.print_from_beginning();
			break;
		default: 
			System.out.println(" Please Enter Correct choice");
			break;
			
		}
		
		
  }//main
   
   
} /* CS401LinkedListImpl<E> */
