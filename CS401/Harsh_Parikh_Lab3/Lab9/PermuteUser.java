import java.util.*;

public class PermuteUser
{
	
	public static void main (String [ ] args)
    {
		new PermuteUser().run();
    }
  public void run(){}
  
  
  public static String permute (String s)
   { 
	  StringBuffer sb=new StringBuffer(s);
	  return recPermute (sb, 0);
   } 
                             
  
  protected static String recPermute (StringBuffer sb, int k)
  {
     if (k == sb.length() - 1)
        return String.valueOf (sb) + "\n";
     else
     {
        String allPermutations = new String();

        
        
        char temp;

        for (int i = k; i < sb.length(); i++)
        {
        	
        	temp=sb.charAt(i);                       //swap ith value into temporary variable
            sb.setCharAt(i, sb.charAt(k));           //swap kth value into i .
            sb.setCharAt(k, temp);                   //swap the temporary value into k.
           	
            StringBuffer sa=new StringBuffer(sb);
            
            allPermutations += recPermute (sa, k + 1);            
        } 
        return allPermutations;
     } 
 } 
  
} 
    
