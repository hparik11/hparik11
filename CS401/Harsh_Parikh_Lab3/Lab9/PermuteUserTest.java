import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.Test;

public class PermuteUserTest {

	@Test
	public void test() {
		
		System.out.println (PermuteUser.permute ("abc"));
		assertEquals(PermuteUser.permute ("abc"), "abc\nacb\nbac\nbca\ncab\ncba\n");  //Comparing the results
	}
	@Test
	public void test1(){ 
		System.out.println (PermuteUser.permute ("1234"));
		assertEquals(PermuteUser.permute ("1234"),"1234\n1243\n1324\n1342\n1423\n1432\n2134\n2143\n2314\n2341\n2413\n2431\n3124\n3142\n3214\n3241\n3412\n3421\n4123\n4132\n4213\n4231\n4312\n4321\n");
		}
	@Test
	public void test2(){ 
		System.out.println (PermuteUser.permute ("wzyx"));
		assertEquals(PermuteUser.permute ("wzyx"),"wzyx\nwzxy\nwyzx\nwyxz\nwxzy\nwxyz\nzwyx\nzwxy\nzywx\nzyxw\nzxwy\nzxyw\nywzx\nywxz\nyzwx\nyzxw\nyxwz\nyxzw\nxwzy\nxwyz\nxzwy\nxzyw\nxywz\nxyzw\n");
		}
	@Test
	public void test3(){ 
		System.out.println (PermuteUser.permute ("abbc"));
		assertEquals(PermuteUser.permute ("abbc"),"abbc\nabcb\nabbc\nabcb\nacbb\nacbb\nbabc\nbacb\nbbac\nbbca\nbcab\nbcba\nbabc\nbacb\nbbac\nbbca\nbcab\nbcba\ncabb\ncabb\ncbab\ncbba\ncbab\ncbba\n");}
	}







