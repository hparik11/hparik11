import java.util.*;

public class FibonacciUser
{
  public static void main (String[] args)
  {
	  new FibonacciUser().First();
  } 

public void First()
{
  
     final int SENTINEL = -1;
 
     final String INPUT_PROMPT = "\nPlease enter the " +
        "positive integer whose Fibonacci number you want (or " +
         SENTINEL + " to quit): ";    
           
     final String FIBONACCI_MESSAGE = "\nIts Fibonacci number is ";
         
     int n;
     final String MESSAGE_1 = "The elapsed time was ";

     final double NANO_FACTOR = 1000000000.0;  // nanoseconds per second

     final String MESSAGE_2 = " seconds.";

     long startTime,
          finishTime,
          elapsedTime;
     System.out.print (INPUT_PROMPT);
     n = new Scanner (System.in).nextInt();
         
     while (true)
     {
    	int previous=1;
    	 int current=1;
        try
        {
        	System.out.println("\n\nMy recursive Version");
        	
          if (n == SENTINEL || n > 92)
           break; 
          startTime = System.nanoTime();
          System.out.println (FIBONACCI_MESSAGE + f(n,previous,current));
          finishTime = System.nanoTime();
          elapsedTime = finishTime - startTime;
          System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);
                  
        }
        catch (Exception e)
        {
          System.out.println (e);
        }   
        
        while (true)
        {
       
           try
           {
        	   System.out.println("\n\nOriginal recursive Version");
        	   
             if (n == SENTINEL)
              break;  
             startTime = System.nanoTime();
             System.out.println (FIBONACCI_MESSAGE + f(n));
             finishTime = System.nanoTime();
             elapsedTime = finishTime - startTime;
             System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);
             
           }
           catch (Exception e)
           {
             System.out.println (e);
           } 
           while (true)
           {
             try
              {
            	 
                System.out.println("\n\nIterative recursive Version");
            	  
                if (n == SENTINEL)
                 break;  
                startTime = System.nanoTime();
                System.out.println (FIBONACCI_MESSAGE + fib(n));
                finishTime = System.nanoTime();
                elapsedTime = finishTime - startTime;
                System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);
                
              }
              catch (Exception e)
              {
                System.out.println (e);
               
              }  
              while (true)
              {
             
                 try
                 {
                	 System.out.println("\n\nFormula recursive Version");
                	 
                   if (n == SENTINEL)
                    break;  
                   startTime = System.nanoTime();
                   System.out.println (FIBONACCI_MESSAGE + fibo(n));
                   finishTime = System.nanoTime();
                   elapsedTime = finishTime - startTime;
                   System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);
                   break;
                 }
                 catch (Exception e)
                 {
                   System.out.println (e);
                  
                 } 
                 break;
              }   
              break;
   }
  break;
  }
 break;   
 }
     
}
 
 public long f(int n, long previous, long current)    //My Recursive Version
 {
	  if(n>2)
	 return f(n-1,current,current+previous);
	 return current;
 }
  
  

public static long f (int n)  						  // Original Recursive Version
{
	final int MAX_N = 92;

        final String ERROR_MESSAGE = "\nThe number entered must be " +
            "greater than 0 and at most " + MAX_N + ".";

        if (n <= 0 || n >= MAX_N)
            throw new IllegalArgumentException (ERROR_MESSAGE);
  	   if (n <= 2 )
    	    return 1;
    	return f(n - 1) + f (n - 2); 
} 

public static long fib (int n)                        //Iterative Version
{
	final int MAX_N = 92;

        final String ERROR_MESSAGE = "\nThe number entered must be " +
            "greater than 0 and at most " + MAX_N + ".";

        long previous,
             current,
             temp;

        if (n <= 0 || n > MAX_N)
            throw new IllegalArgumentException (ERROR_MESSAGE);
  	if (n <= 2) 
    		return 1;
  	previous = 1;
  	current = 1;
  	for (int i = 3; i <= n; i++) 
  	{
    		temp = current;
    		current = current + previous;
    		previous = temp;    
  	} 
  	return current;
}

public static long fibo(int n)                       //Formula Version
{
	final int MAX_N = 92;

        final String ERROR_MESSAGE = "\nThe number entered must be " +
            "greater than 0 and at most " + MAX_N + ".";

        if (n <= 0 || n > MAX_N)
            throw new IllegalArgumentException (ERROR_MESSAGE);
  
  	return (long)((1 / Math.sqrt (5)) *
                (Math.pow((1 + Math.sqrt (5)) / 2, n)- 
           	Math.pow((1 - Math.sqrt (5)) / 2, n)));
} 

}