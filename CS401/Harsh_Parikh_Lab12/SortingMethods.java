package Lab12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class SortingMethods {
	public static void main(String[] args) throws FileNotFoundException{
		File f = new File("students.dat");
		//please update the path for file students.dat as per your local workspace and file path
		String readLine;
		String[] readLineSplited;
		studentsdata Quicksort[]= new studentsdata[3380];
		//Reading data into array----------
		int i=0;
		Scanner scan = new Scanner(f);
		while(scan.hasNextLine()){	
			readLine = scan.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata student1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			Quicksort[i]= student1;
			i=i+1;
		}
		long startTime = System.nanoTime();
		QuickSort.quickSort(Quicksort);
		long elepsedTime = System.nanoTime() - startTime;
		double seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Quick Sort of data file takes "+seconds+" miliseconds");
		scan.close();
		
		studentsdata Mergesort[]= new studentsdata[3380];
		Scanner scan2 = new Scanner(f);
		//Reading data into array----------
		i=0;
		while(scan2.hasNextLine()){	
			readLine = scan2.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata student1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			Mergesort[i]= student1;
			i=i+1;
		}	
		startTime = System.nanoTime();
		MergeSort.mergeSort(Mergesort);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Mearge Sort of data file takes "+seconds+" miliseconds");
		scan2.close();
		
		
		studentsdata Heapsort[]= new studentsdata[3380];
		Scanner scan3 = new Scanner(f);
		//Reading data into array----------
		i=0;
		while(scan3.hasNextLine()){	
			readLine = scan3.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata student1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			Heapsort[i]= student1;
			i=i+1;
		}	
		startTime = System.nanoTime();
		HeapSort.heapSort(Heapsort);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Heap Sort of data file takes "+seconds+" miliseconds");
		scan3.close();
		}
}