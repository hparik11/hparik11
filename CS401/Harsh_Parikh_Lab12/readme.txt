
The sourse(src) contains java files for sortings and comparision done. "students.dat" is a data file provided for input.

--"studentsdata.java" is a class file with defination of compareTo method of Comparable interface. It compares based on last name, first name middle name (and if all these are same, student id).

--"HeapSort.java", "QuickSort.java" and "MeargeSort.java" are class file contains sorting algorithms for HeapSort, QuickSort and MeargeSort. Each of these files have methods which take array object as an input and performs sorting on them. It uses comparable interface. 
 
--"SortingMethod.java" is a main file for sorting comparision. We first read all data from DAT file into an array and then performs Quicksort, Heapsort and Meargesort with time calculations. 