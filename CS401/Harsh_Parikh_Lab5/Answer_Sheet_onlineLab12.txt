1) if (c == null)
   throw new NullPointerException();

Then the test method that checks for NullPointerException should succeed. What if you comment out the above two lines (if (c == null) ...)? Does the test still succeed?


Ans-- Yes, If I comment out the above lines , the test still succeed. c == null is directly comparing with null with "==". So there is no point to put that line in addAll(). 


2)Note any discrepancies between what you hypothesized and what you discovered during testing.

Ans- There is no discrepancies. 



 