import java.util.*;
import java.util.Iterator;
import java.util.LinkedList;




public class PE7_2 {
	

  
	
	public static void main(String args[])
	{
		LinkedList<Character> letters = new LinkedList<Character> ();
		ListIterator<Character> itr = letters.listIterator();
		itr.add ('f');                         //add letter 'f'
		itr.add ('t');                         //add letter 't'
		itr.previous();                        //iterator pointing on 't'
		itr.previous();			       //iterator pointing on 'f'
		
		//System.out.println(itr.previous());
		//System.out.println(itr.next());
		itr.add ('e');			       //add letter before 'f'
		itr.add ('r');                         //add letter 'r'
		itr.next();                            //iterator pointing after 'f'
		itr.add ('e');                         //add letter 'e'
		itr.add ('c');                         //add letter 'c'
		itr = letters.listIterator();          //iterator pointing to first element
		itr.add ('p');				//add letter in the beginning
		System.out.println (letters);          //print all letters.. Output-- [p,e,r,f,e,c.t]
	}
}
