

import java.util.LinkedList;
import java.util.ListIterator;

public class PE7_6 {
	
	public static void main (String[] args){
		LinkedList<Double> weights = new LinkedList<Double>();      
		ListIterator<Double> itr;
		weights.add(5.3);                           //add 5.3 in the beginning
		weights.add(2.8);			    //add 2.8 after 5.3
		itr = weights.listIterator();               // iterator will start from first position.
		System.out.println(weights);                //Output--[5.3,2.8]
		
		/*     A      */
		
		itr.add (8.8);                              //add 8.8 in the beginning         
		itr.next();                                 //pointer is pointing after 5.3
		itr.remove();                               //remove 5.3 from list
		
		System.out.println(weights);                // Output-- [8.8,2.8]
		
		/*     B      */
		
		itr.add (8.8);                              //add 8.8
		itr.remove();                               //Give error.
		itr.next();
		
		System.out.println(weights);                //Error. Illegal Exception. cannot perform on the current position of the iterator.
		
		/*     C      */
		
		itr.next(); 
		itr.add (8.8); 
		itr.remove();
		
		System.out.println(weights);                //Error..Illegal Exception. cannot perform on the current position of the iterator.
		
		
		/*     D      */
		itr.next();                                 //iterator points after 5.3
		itr.remove();                               //remove 5.3
		itr.add (8.8);                              //add 8.8
		
		System.out.println(weights);                //Output---[8.8,8.8]
		
		/*     E      */
		itr.remove(); 
		itr.add (8.8); 
		itr.next();
		
		System.out.println(weights);                //Error.. Illegal Exception. cannot perform on the current position of the iterator.
		
		/*     F      */
		
		itr.remove(); 
		itr.next(); 
		itr.add (8.8);
	    System.out.println(weights);                //Error.. Illegal Exception. cannot perform on the current position of the iterator.
		
	}
}
