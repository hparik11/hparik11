
import java.util.ArrayList;

import java.util.*;

public class SinglyLinkedList<E> extends AbstractCollection<E> 
  implements List<E>
{
  protected Entry<E> head;
  
  
  /**
   * Initializes this SinglyLinkedList object to be empty, with elements to be of 
   *  type E.
   */
  public SinglyLinkedList()
  {
    head = null;
  } // constructor
  
  
  /**
   *  Determines if this SinglyLinkedList object has no elements.
   *
   *  @return true - if this SinglyLinkedList object has no elements; otherwise,
   *                          false.  
   */
  public boolean isEmpty ()  
  {
    return head == null;
  } // method isEmpty
  
  
  /**
   *  Adds a specified element to the front of this SinglyLinkedList object.
   *
   *  @param e - the element to be prepended.
   *
   */
  public void addToFront (E element) 
  {
    Entry<E> temp = new Entry<E>();
    temp.element = element;
    temp.next = head;
    head = temp;
  } // method addToFront
  
  
  /**
   *  Returns a SinglyLinkedListIterator object to iterate over this
   *  SinglyLinkedList object.
   *
   */  
  public Iterator<E> iterator()
  {
    return new SinglyLinkedListIterator();
  } // method iterator
  
  
  /**  
   *  Determines the number of elements in this SinglyLinkedList object.
   *  The worstTime(n) is O(n).
   *
   *  @return the number of elements.
   */
  public int size() 
  {
    int count = 0;
    
    for (Entry<E> current = head; current != null; current = current.next)
      count++;
    return count;
  } // method size
  
  
  /** 
   *  Determines if this SinglyLinkedList object contains a specified element.
   *  The worstTime(n) is O(n).
   *
   *  @param obj - the specified element being sought.
   *
   *  @return true - if this SinglyLinkedList object contains element; otherwise,
   *                false. 
   *
   */
  public boolean contains (Object obj) 
  {
    if (obj == null)
    {
      for (Entry<E> current = head; current != null; current = current.next)
        if (current.element == null)
          return true;
    } // if obj == null
    else   
      for (Entry<E> current = head; current != null; current = current.next)
        if (obj.equals (current.element))
          return true;
    return false;
  } // method contains
  

  public ListIterator<E> listIterator(int index)
  {
    throw new UnsupportedOperationException( );
  }  
 
  public ListIterator<E> listIterator()
  {
    throw new UnsupportedOperationException( );
  }

  public int lastIndexOf(Object obj)
  {
    throw new UnsupportedOperationException( );
  }

  public int indexOf(Object obj)
  {
    throw new UnsupportedOperationException( );
  }

  public E remove(int index)
  {
    throw new UnsupportedOperationException( );
  }

  
  public boolean add(E element)
  {
    throw new UnsupportedOperationException( );
  } 

  public void add(int index, E element)
  {
    throw new UnsupportedOperationException( );
  } 

  public E set(int index, E element)
  {
    throw new UnsupportedOperationException( );
  }

  public E get(int index)
  {
    throw new UnsupportedOperationException( );
  }

  /*
  {
    throw new UnsupportedOperationException( );
  }
  */
  
  public boolean addAll(int index, Collection<? extends E> c)
  {
    throw new UnsupportedOperationException( );
  }

  public Object[] toArray() 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public <E>E[] toArray(E[] a)
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public boolean remove(Object obj) 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public boolean containsAll(Collection<?> c) 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  
  public boolean removeAll(Collection<?> c) 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public boolean retainAll(Collection<?> c) 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public List<E> subList(int fromIndex, int toIndex)
  { 
    throw new UnsupportedOperationException( ); 
  }
  public void clear() 
  {
    head = null; 
  }
  
  public boolean equals(Object o) 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  public int hashCode() 
  { 
    throw new UnsupportedOperationException( ); 
  }
  
  
  protected class SinglyLinkedListIterator implements Iterator<E> 
  {
    protected Entry<E> next;
    
    /**
     *  Initializes this SinglyLinkedListIterator object
     */
    SinglyLinkedListIterator() 
    {
      next = head;
    } // constructor
    
    
    /** 
     *  Returns the element this SinglyLinkedListIterator object was 
     *  (before this call) positioned at, and advances this 
     *  SinglyLinkedListIterator object.
     *                    
     *  @return - the element this SinglyLinkedListIterator object was 
     *            positioned at.
     *
     *  @throws NullPointerException � if this Iterator object was
     *                 not postioned at an element before this call.
     */                            
    public E next() 
    {
     
    	E theElement = next.element;
      next = next.next;
      return theElement;
    } // method next
    
    
    /**
     *  Determines if this SinglyLinkedListIterator object is positioned 
     * at an element in this SinglyLinkedList object.
     *
     *  @return true - if this SinglyLinkedListIterator object is 
     *                 positioned at an element; otherwise, false.
     */                   
    public boolean hasNext() 
    {       
      return next != null;
    } // method hasNext
    
    /**
     *  Removes the element returned by the most recent call to next().
     *  The behavior of this Iterator object is unspecified if the underlying 
     *  collection is modified (while this iteration is in progress) other than 
     *   by calling this remove() method.
     *
     *  @throws IllegalStateException - if next() had not been called before
     *                 this call to remove(), or if there had been an intervening call 
     *                 to remove() between the most recent call to next() and this 
     *                 call.
     */
    public void remove() 
    { 
      throw new UnsupportedOperationException( ); 
    }
    
  } // class SinglyLinkedListIterator
  
  
  protected static class Entry<E> 
  {
    E element;
    Entry<E> next;
    
  } // class Entry
  
  
  /**
   *  Adds all elements of a specified collection to the front of the 
   *  SinglyLinkedList object. The elements will be in the reverse of the
   *  order they were in the collection. 
   * 
   *  @param c - a Collection<? extends E> object whose elements 
   *         will be added to the calling SinglyLinkedList object.
   * 
   *  @return a boolean indicating whether the SinglyLinkedList object  
   *          has been changed as a result of this call.
   *
   *  @throws NullPointerException - if c is null.
   */
  public boolean addAll(Collection<? extends E> c){
	  	 
	  		  
		  Iterator<? extends E> itr = c.iterator();
		 // int a = c.size();
		   while (itr.hasNext( )) 
		  {
			   addToFront(itr.next()); 
			   	
		  } // while
		  return false;
  }
	  	  
	  	  
  
  
	public static void main(String[] args){
			
			
			SinglyLinkedList<Chores> fullList = new SinglyLinkedList<Chores>();     //Super Class
			Chores Chores_initiator = new Chores("Play golf", 20);
			fullList.addToFront (Chores_initiator);         
			
			ArrayList<Chores> chores_list = new ArrayList<Chores>();                //Sub-Class
			Chores a = new Chores("Make Bed", 10);
			Chores b = new Chores("Do Laundry", 25);
			Chores c = new Chores("Take out garbage", 80);
			Chores d = new Chores("Clean car", 18);
			Chores e = new Chores("Excercise", 20);
			Chores f = new Chores("Sleep", 40);
			Chores g = new Chores("Bye",200);
			
			chores_list.add(a); 
			chores_list.add(b); 
			chores_list.add(c);
			chores_list.add(d);
			chores_list.add(e);
			chores_list.add(f);
			chores_list.add(g);
			
			System.out.println("There are " + chores_list.size() +
			             " elements on the chores list!");

			
			fullList.addAll (chores_list);
			System.out.println("There are total "+ fullList.size() +" Elements in my list. \n");
			System.out.println(fullList);
			
			
			
		}

  
  
} // class SinglyLinkedList
