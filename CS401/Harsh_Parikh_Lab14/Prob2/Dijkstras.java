package Lab14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Dijkstras {
	private int distances[];
	private Set<Integer> settled;
	private Set<Integer> unsettled;
	private int noOfNodes;
	private int inputMatrix[][];
	private int predecessorNode[];

	public Dijkstras(int noOfNodes) {
		this.noOfNodes = noOfNodes;
		distances = new int[noOfNodes + 1];
		settled = new HashSet<Integer>();
		unsettled = new HashSet<Integer>();
		inputMatrix = new int[noOfNodes + 1][noOfNodes + 1];
		predecessorNode = new int[noOfNodes + 1];
	}

	public void execute(int martix[][], int source) {
		int evaluationNode;
		for (int i = 1; i <= noOfNodes; i++)
			for (int j = 1; j <= noOfNodes; j++)
				inputMatrix[i][j] = martix[i][j];

		for (int i = 1; i <= noOfNodes; i++) {
			distances[i] = Integer.MAX_VALUE;
			predecessorNode[i] = source;
		}

		unsettled.add(source);
		distances[source] = 0;
		while (!unsettled.isEmpty()) {
			evaluationNode = getLowestCostNode();
			unsettled.remove(evaluationNode);
			settled.add(evaluationNode);
			evaluateNeighbours(evaluationNode);
		}
	}

	private int getLowestCostNode() {
		int min;
		int node = 0;

		Iterator<Integer> iterator = unsettled.iterator();
		node = iterator.next();
		min = distances[node];
		for (int i = 1; i <= distances.length; i++) {
			if (unsettled.contains(i)) {
				if (distances[i] <= min) {
					min = distances[i];
					node = i;
				}
			}
		}
		return node;
	}

	private void evaluateNeighbours(int evaluationNode) {
		int edgeDistance = -1;
		int newDistance = -1;

		for (int destinationNode = 1; destinationNode <= noOfNodes; destinationNode++) {
			if (!settled.contains(destinationNode)) {
				if (inputMatrix[evaluationNode][destinationNode] != Integer.MAX_VALUE) {
					edgeDistance = inputMatrix[evaluationNode][destinationNode];
					newDistance = distances[evaluationNode] + edgeDistance;
					if (newDistance < distances[destinationNode]) {
						distances[destinationNode] = newDistance;
						predecessorNode[destinationNode] = evaluationNode;
					}
					unsettled.add(destinationNode);
				}
			}
		}
	}

	public static void main(String args[]) {
		int adjacency_matrix[][];
		int nodes;
		int source = 0;
		Scanner scan;
		Scanner scan1;
		try {
			/*Please specify location of 
			 *your file with topological  
			 *structure of nodes for calculating  
			 *structure of nodes for calculating */
			File file = new File("C://Users//Harsh_laptop//workspace//CS401//src//Lab14//graph2.txt");
			scan = new Scanner(file);
			scan1 = new Scanner(System.in);

			System.out.println("Enter the maximum number of vertices: ");
			nodes = scan1.nextInt();

			adjacency_matrix = new int[nodes + 1][nodes + 1];
			System.out.println("The Weighted Matrix for the graph");
			String nodeNumber = "   ";
			for (int k = 1; k <= nodes; k++) {
				nodeNumber = nodeNumber+ k+" ";
			}
			System.out.println(nodeNumber);
			System.out.println("-------------");
			for (int i = 1; i <= nodes; i++) {
				System.out.print(i+" |");
				for (int j = 1; j <= nodes; j++) {
					adjacency_matrix[i][j] = scan.nextInt();
					if (i == j) {
						adjacency_matrix[i][j] = 0;
						System.out.print(adjacency_matrix[i][j] + " ");
						continue;
					} else if (adjacency_matrix[i][j] == 0) {
						adjacency_matrix[i][j] = Integer.MAX_VALUE;
						System.out.print("0 ");
					} else {
						System.out.print(adjacency_matrix[i][j] + " ");
					}
				}
				System.out.println();
			}

			System.out.println("\n\nPlease enter a start vertex number (from 1 to "+ nodes +"): ");
			source = scan1.nextInt();

			Dijkstras dijkstrasAlgorithm = new Dijkstras(nodes);
			dijkstrasAlgorithm.execute(adjacency_matrix, source);
			int destination = 1;
			for (int i = 1; i <= nodes; i++) {
				if (i != source) {
					ArrayList<Integer> path = new ArrayList<Integer>();
					destination = i;
					path.add(destination);
					while (dijkstrasAlgorithm.predecessorNode[destination] != source) {
						path.add(dijkstrasAlgorithm.predecessorNode[destination]);
						destination = dijkstrasAlgorithm.predecessorNode[destination];
					}
					path.add(source);

					Collections.reverse(path);
					System.out
							.println("From Node " + source
									+ " to Node " + i + "   ----->"
									+ " Cost is "
									+ dijkstrasAlgorithm.distances[i]+ ", Path is" + path.toString());
				}
			}
		} catch (InputMismatchException inputMismatch) {
			System.out.println("Wrong Input Format");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}