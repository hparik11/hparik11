package Lab14;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class Graph 
{
	private ArrayList<Vertex> vertex;
	
	public Graph() {
		vertex = new ArrayList<>();
	}
	
	public void add(Vertex v) {
		vertex.add(v);
	}
	
	public Vertex getVertex(String name) {
		for (Vertex vertex : vertex) {
			if (vertex.getName().equals(name)) {
				return vertex;
			}
		}
		throw new RuntimeException("No Vertex : " + name);
	}
	
	public ArrayList<Vertex> getVertexs() {
		return vertex;
	}
	
	public void bfs(String vertex) {
		Queue<Vertex> queue = new LinkedList<Vertex>();
		queue.add(getVertex(vertex));
		Vertex v = null, dest = null;
		PriorityQueue<Edge> pqEdges = null;
		Edge edge = null;
		while (!queue.isEmpty()) {
			v = queue.remove();
			if (v.getState() != 2) {
				this.visitVertex(v);
			}
			pqEdges = v.getPriorityEdges();
			while (!pqEdges.isEmpty()) {
				edge = pqEdges.remove();
				dest = edge.getDestination();
				if (dest.getState() == 0) {
					dest.setState(1);
					queue.add(dest);
				}
			}
		}
	}
	
	public void dfs(String vertex) {
		Stack<Vertex> stack = new Stack<>();
		Stack<Edge> stackEdges = new Stack<>();
		
		stack.add(getVertex(vertex));
		Vertex v = null, dest = null;
		PriorityQueue<Edge> pqEdges = null;
		Edge edge = null;
		while (!stack.isEmpty()) {
			v = stack.pop();
			if (v.getState() != 2) {
				this.visitVertex(v);
			}
			pqEdges = v.getPriorityEdges();
			while (!pqEdges.isEmpty()) {
				stackEdges.push(pqEdges.remove());
			}
			while (!stackEdges.isEmpty()) {
				edge = stackEdges.pop();
				dest = edge.getDestination();
				if (dest.getState() != 2) {
					dest.setState(1);
					stack.add(dest);
				}
			}
		}
	}
	
	protected void visitVertex(Vertex v) {
		v.setState(2);
		System.out.print(v.getName() + " ");
	}
	
	protected void discoverVertex(Vertex v) {
		v.setState(1);
	}
	
	public void createMinimumSpanningTree(Vertex startV) {
		
		for (Vertex vertex : this.vertex) {
			vertex.setConnected(false);
		}
		
		System.out.println("Start vertex:" + startV.getName());
		
		PriorityQueue<Edge> pq = new PriorityQueue<>();
		ArrayList<Vertex> connectedVertexs = new ArrayList<>();
		startV.setConnected(true);
		connectedVertexs.add(startV);
		
		ArrayList<Edge> neighborEdges = startV.getEdges();
		for (Edge edge : neighborEdges) {
			pq.add(edge);
		}
	
		int totalCost = 0;
		while (connectedVertexs.size() < this.vertex.size()) {
			if (pq.isEmpty()) {
				break;
			}
			Edge edge = pq.remove();
			if (edge.getSource().isConnected() && !edge.getDestination().isConnected()) {
				
				System.out.println("  Add edge <" + edge.getSource().getName() +
						", " + edge.getDestination().getName() + ", " + (double)edge.getCost() + ">");
				totalCost += edge.getCost();
				edge.getDestination().setConnected(true);
				connectedVertexs.add(edge.getDestination());
				neighborEdges = edge.getDestination().getEdges();
				for (Edge nerghborEdge : neighborEdges) {
					if (edge.getDestination() != edge.getSource()) {
						pq.add(nerghborEdge);
					}
				}
			}
		}
		
		System.out.println("Total cost: " + totalCost);
		
	}
	
}
