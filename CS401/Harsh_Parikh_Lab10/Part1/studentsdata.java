package Lab10;

public class studentsdata implements Comparable<studentsdata> {

	  String fname;
	  String lname;
	  String mname;
	  long studentId;
	  
	  public studentsdata (String lname,String fname,String mname,long studentId)
	  {
	    this.fname = fname;
	    this.lname = lname;
	    this.mname = mname;
	    this.studentId = studentId;
	  } // constructor
	  
	  public int compareTo (studentsdata otherStudent)
	  {
	    //final double DELTA = 0.0000001;
	    
	    if (studentId < otherStudent.studentId )
	      return -1;
	    else if (studentId > otherStudent.studentId )
	      return 1;
	    else if (studentId == otherStudent.studentId )
	      return 0;
	    else return (Integer) null;
	    } // method compareTo

}

