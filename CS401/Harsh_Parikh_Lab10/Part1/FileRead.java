package Lab10;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;


import Lab10.BinarySearchTree.Entry;

   public class FileRead  {


public static void main (String[] args) throws IOException{
		
	CS401LinkedListImpl<studentsdata> RandomLinkList = new CS401LinkedListImpl<studentsdata>();
	CS401LinkedListImpl<studentsdata> SortedLinkList = new CS401LinkedListImpl<studentsdata>();
		BinarySearchTree<studentsdata> studentsdataBST = new BinarySearchTree<studentsdata>(); 
		
		File f = new File("students.dat");
		
		String readLine;
		String[] readLineSplited;
		Scanner scan = new Scanner(f);
		
		
		//Reading for Linked List
		long startTime = System.nanoTime();
		while(scan.hasNextLine()){	
			readLine = scan.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			RandomLinkList.add(studentsdata1);
		}
		long elepsedTime = System.nanoTime() - startTime; 
		double seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Time taken to load the records in a linked list: "+ (seconds) +" miliseconds");
		//System.out.println("Size : "+RandomLinkList.size());
		scan.close();
		
		Scanner scan2 = new Scanner(f);
		long startTime2 = System.nanoTime();
		while(scan2.hasNextLine()){	
			readLine = scan2.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			SortedLinkList.add_sorted(studentsdata1);
		}
		long elepsedTime2 = System.nanoTime() - startTime2; 
		double sort_sec = (double)elepsedTime2 / 1000000.0;
		System.out.println("Time taken to load the records in a sorted linked list: "+ (sort_sec) +" miliseconds");
		//System.out.println("Size : "+RandomLinkList.size());
		scan.close();
		
		//reading for binary search tree
		Scanner scan3 = new Scanner(f);
		long startTime3 = System.nanoTime();
		while(scan3.hasNextLine()){	
			readLine = scan3.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			studentsdataBST.add(studentsdata1);
		}
		long elepsedTime3 = System.nanoTime() - startTime3; 
		seconds = (double)elepsedTime3 / 1000000.0;
		System.out.println("Time taken to load the records in a binary search tree : "+ (seconds) +" miliseconds");
		//System.out.println("Size : "+ studentsdataBST.size);
		
		scan3.close();	
		
		
		System.out.println("\n\n---(1)---");
		System.out.println("Searching for studentsdata ID 483293267");
		studentsdata s1 = new studentsdata(null, null, null, 483293267);
		studentsdata temp = null;
		startTime = System.nanoTime();
		for(studentsdata k : RandomLinkList){
			if(k.studentId == s1.studentId)
				temp =k;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		startTime = System.nanoTime();
		Entry<studentsdata> sResult = studentsdataBST.getEntry(s1);
		System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");
		
		
		System.out.println("\n\n---(2)---");
		System.out.println("Searching for studentsdata ID 1902997270");
		s1 = new studentsdata(null, null, null, 1902997270);
		temp = null;
		startTime = System.nanoTime();
		for(studentsdata i : RandomLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " +temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		sResult = studentsdataBST.getEntry(s1);
		System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");
		
		System.out.println("\n\n---(3)---");
		System.out.println("Searching for studentsdata ID 856408684");
		s1 = new studentsdata(null, null, null,856408684);
		temp = null;
		startTime = System.nanoTime();
		for(studentsdata i : RandomLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " +temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		sResult = studentsdataBST.getEntry(s1);
		if(sResult==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");

		
		System.out.println("\n\n---(4)---");
		System.out.println("Searching for studentsdata ID 143507366");
		s1 = new studentsdata(null, null, null,143507366);
		temp = null;
		startTime = System.nanoTime();
		for(studentsdata i : RandomLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " +temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		sResult = studentsdataBST.getEntry(s1);
		if(sResult==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");

		
		System.out.println("\n\n---(5)---");
		System.out.println("Searching for studentsdata ID 307954472");
		s1 = new studentsdata(null, null, null,307954472);
		temp = null;
		startTime = System.nanoTime();
		for(studentsdata i : RandomLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " +temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		sResult = studentsdataBST.getEntry(s1);
		if(sResult==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");

		
		System.out.println("\n\n---(6)---");
		System.out.println("Searching for studentsdata ID 876561221");
		s1 = new studentsdata(null, null, null,876561221);
		temp = null;
		startTime = System.nanoTime();
		for(studentsdata i : RandomLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " +temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a random linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		for(studentsdata i : SortedLinkList){
			if(i.studentId == s1.studentId)
				temp =i;
		}
		if(temp==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(temp.lname+ "  " + temp.fname + temp.mname + "  " + temp.studentId);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a sorted linked list : "+ (seconds) +" miliseconds");
		
		
		
		startTime = System.nanoTime();
		sResult = studentsdataBST.getEntry(s1);
		if(sResult==null){
			System.out.println("No record with this key found, studentsdata not found");
		}else
			System.out.println(sResult.element.lname+ "  " + sResult.element.fname + sResult.element.mname + "  " + sResult.element.studentId);
		
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("- Searching time in a binary search tree : "+ (seconds) +" miliseconds");




}
}  