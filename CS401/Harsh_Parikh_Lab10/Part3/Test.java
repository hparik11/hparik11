package Lab10;

import java.util.Comparator;

public class Test
{
   public static void main(String[] args)
   {
      Student s1 = new Student("Harsh 3.8"),
              s2 = new Student("Raj 3.8");
      int c;

      System.out.println("Natural ordering considers GPA ");
      System.out.println("Using natural ordering and comparing\n" +
                         s1 + " == " + s2);
      c = s1.compareTo(s2);
      
      if (c == 0) 
         System.out.println("These objects are the same.");
      else
         System.out.println("These objects are different.");
 

      System.out.println();

      System.out.println("Unnatural ordering considers names ");
      System.out.println("Using unnatural ordering and comparing\n" + s1 + " == " + s2);

      Comparator<Student> nm_compare = new ByName();
    
      c = nm_compare.compare(s1, s2);

      if (c == 0) 
         System.out.println("These objects are the same.");
      else
         System.out.println("These objects are different.");
   }
}
