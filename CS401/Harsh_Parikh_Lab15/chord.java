package chord;

public class chord {
	
	public static void main(String[] args){
		long node = h("10.0.0.1");
		System.out.println(node);
		
	}
	
	public class Chord_node  {

		   private static final int m = 8;

		   Chord_node successor;    // Node's successor
		   Chord_node predecessor;  // Node's predecessor
		   Chord_node[] finger_table;  // Finger table of at most m entries
		   int node_index;     // Integer index of this node [0 - 255]
		   String node_name;   // Name of node, e.g., N20 or N190

		}
	
	
	 public static long h(String s)  {
	      long h = 0;
	      int len = s.length();

	      for (int i = 0; i < len; i++)  {
	        h = 31 * h + s.charAt(i);
	      }

	      return Math.abs(h % 256);  // In case of overflow and negative numbers
	   }
	 

}
