package chord;

import java.util.Arrays;

public class chord1 {

	public static int h(String s)  {
		long h = 0;
		int len = s.length();

		for (int i = 0; i < len; i++)  {
			h = 31 * h + s.charAt(i);
		}

		return (int) Math.abs(h % 256);  // In case of overflow and negative numbers
	}


	public static class Chord_node  {

		private static int m = 8;

		Chord_node successor;    // Node's successor
		Chord_node predecessor;  // Node's predecessor
		Chord_node[] finger_table;  // Finger table of at most m entries
		int node_index;     // Integer index of this node [0 - 255]
		String node_name;   // Name of node, e.g., N20 or N190
	}

	public static void buildNodes(String[] node_IPs, Chord_node[] nodeArr)
	{
		Chord_node node;
		int i =0;
		int[] aux = new int[node_IPs.length];

		for(String a : node_IPs )
		{
			aux[i] = h(a);
			i++;
		}

		Arrays.sort(aux);

		i=0;
		for(int a : aux )
		{

			System.out.println("The given hash input is :" +a);
			String p ="N";
			p=p.concat(Integer.toString(a));
			System.out.println("the node out of "+a+ " is "+p);

			Chord_node n = new Chord_node();
			n.node_name = p;
			n.node_index = a;
			nodeArr[i] = n;
			i++;
		}

	}

	public static void build_FingerTable_and_Connections (Chord_node[] nodeArr)
	{
		for(int i=0; i<nodeArr.length; i++)
		{
			if(i!=0)
			{
				nodeArr[i].predecessor = nodeArr[i-1];
			}
			if(i!=nodeArr.length-1)
			{
				nodeArr[i].successor = nodeArr[i+1];
			}

		}
		for (Chord_node a : nodeArr)
		{

		}
	}

	public static void main (String[] args)
	{
		String[] node_IPs = {"10.0.0.1",
				"12.0.5.1",
				"198.165.98.11",
				"198.165.98.201",
				"0.0.0.1",
				"0.0.0.10",          
				"216.47.143.249",    
				"216.47.152.222",    
				"98.138.253.109",    
				"98.139.183.24",     
				"98.138.252.30",     
				"75.75.7.101",       
				"85.75.7.101",       
				"151.207.17.241",    
				"10.48.71.91",       
				"15.89.38.55",       
				"215.147.210.28",    
				"10.0.251.128",      
				"69.89.31.5",        
				"9.8.31.5",          
				"64.188.59.23",      
				"74.6.50.24",        
				"23.45.174.184",     
				"23.75.217.84",      
		"128.239.155.101"}; 
		System.out.println(node_IPs.length);
		Chord_node[] nodeArr = new Chord_node[node_IPs.length];
		System.out.println(nodeArr.length);
		buildNodes(node_IPs, nodeArr);
		for(Chord_node a : nodeArr)
			System.out.println(a.node_name);




	}



}
