package Lab11;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MainSort {
	public static void main(String[] args) throws FileNotFoundException{
		
		int arrayLength = 3380;
		File f = new File("students.dat");
		
		String readLine;
		String[] readLineSplited;
		studentsdata DataInsert[]= new studentsdata[arrayLength];
		Scanner scan = new Scanner(f);
		
		
		//Reading data into array----------
		int i=0;
		while(scan.hasNextLine()){	
			readLine = scan.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			DataInsert[i]= studentsdata1;
			i=i+1;
		}
		
		
		long startTime = System.nanoTime();
		ComparableSorts.insertionSort(DataInsert);
		long elepsedTime = System.nanoTime() - startTime;
		double seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Insertion sort of data file takes "+seconds+" miliseconds");
		scan.close();
		
		studentsdata DataSelection[]= new studentsdata[arrayLength];
		Scanner scan2 = new Scanner(f);
		//Reading data into array----------
		i=0;
		while(scan2.hasNextLine())
		{	
			readLine = scan2.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			DataSelection[i]= studentsdata1;
			i=i+1;
		}	
		
		
		startTime = System.nanoTime();
		ComparableSorts.selectionSort(DataSelection);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Selection sort of data file takes "+seconds+" miliseconds");
		scan2.close();
		
		studentsdata DataBubbleSort[]= new studentsdata[arrayLength];
		Scanner scan3 = new Scanner(f);
		//Reading data into array----------
		i=0;
		while(scan3.hasNextLine()){	
			readLine = scan3.nextLine();
			readLineSplited= readLine.split(" ");
			studentsdata studentsdata1 = new studentsdata(readLineSplited[0] , readLineSplited[1], readLineSplited[2], Long.parseLong(readLineSplited[3])); 
			DataBubbleSort[i]= studentsdata1;
			i=i+1;
		}	
		
		startTime = System.nanoTime();
		ComparableSorts.bubbleSort(DataBubbleSort);
		elepsedTime = System.nanoTime() - startTime;
		seconds = (double)elepsedTime / 1000000.0;
		System.out.println("Bubble sort of data file takes "+seconds+" miliseconds");
		scan3.close();
		
				
		
	}
}
