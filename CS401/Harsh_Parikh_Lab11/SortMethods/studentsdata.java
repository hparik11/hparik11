package Lab11;

public class studentsdata implements Comparable<studentsdata> {

	  String fname;
	  String lname;
	  String mname;
	  long studentId;
	  
	  public studentsdata (String lname,String fname,String mname,long studentId)
	  {
	    this.fname = fname;
	    this.lname = lname;
	    this.mname = mname;
	    this.studentId = studentId;
	  } // constructor
	  
	  public int compareTo (studentsdata otherStudent)
	  {
	    //final double DELTA = 0.0000001;
		  String fullName = lname+fname+mname;
		  String OtherFullName = otherStudent.lname+otherStudent.fname+otherStudent.mname;
	    
		if (fullName.compareTo(OtherFullName) < 0 )
			return -1;
		else if (fullName.compareTo(OtherFullName) > 0 )
			return 1;
		else if (fullName.compareTo(OtherFullName) == 0 )
		{
			if (studentId < otherStudent.studentId )
			      return -1;
			    else if (studentId > otherStudent.studentId )
			      return 1;
			    else 
					return (Integer) null;
		}
		else 
			return (Integer) null;
			
		  
		/*if (studentId < otherStudent.studentId )
	      return -1;
	    else if (studentId > otherStudent.studentId )
	      return 1;
	    else if (studentId == otherStudent.studentId )
	      return 0;
	    else return (Integer) null;*/
	    } // method compareTo


}

