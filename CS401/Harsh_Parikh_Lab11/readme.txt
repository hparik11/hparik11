

The sourse(src) contains java files for sortings, their testing and comparision done. "students.dat" is a data file provided for input.

--"studentsdata.java" is a class file with defination of compareTo method of Comparable interface. It compares based on  last name, first name middle name (and if all these are same, student id).

--"ComparableSorts.java" is a class file contains sorting algorithms for insertionSort, selectionSort and bubbleSort. These methods take array object as an input and performs sorting on them. It uses comparable interface. 
"ComparableSortsTest.java" is a test class for "ComparableSorts.java". 
 
--"MainSort.java" is a main file for these sorting comparision. We first read all data from DAT file into an array and then performs selection, insertion and bubble sort with time calculations. It shows the algorithms taking O(n^2) time. We can also say from the output that which algorithm performs better. 