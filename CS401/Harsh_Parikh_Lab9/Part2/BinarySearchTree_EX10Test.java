package Lab9;

import static org.junit.Assert.*;
import static org.junit.runner.JUnitCore.runClasses;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Result;

public class BinarySearchTree_EX10Test {

	public static void main(String[ ] args)
    {  
        Result result = runClasses (BinarySearchTreeTest.class);
        System.out.println ("Tests run = " + result.getRunCount() +
                            "\nTests failed = " + result.getFailures());
    } // method main

    protected BinarySearchTree_EX10<String> tree1;          
                       
    @Before    
    public void RunBeforeEachTest()
    {
        tree1 = new BinarySearchTree_EX10<String>();  
        
    } // method RunBeforeEachTest
 
    @Test
    public void testAdd() throws Exception 
    {                       
    	tree1.add_recur("b");
    	tree1.add_recur("a");
    	tree1.add_recur("c");
    	tree1.add_recur("d");
    	tree1.add_recur("a");
    	System.out.println(tree1.height());
        assertEquals (true, tree1.contains ("a")); 
        assertEquals(2,tree1.height());
        
        //System.out.println(tree1.size());
      //  assertEquals ("[]", tree.toString());       
    } // method testDefaultConstructor

}
