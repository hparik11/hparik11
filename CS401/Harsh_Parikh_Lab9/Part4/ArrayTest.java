package Lab9;

public class ArrayTest {

	public static void main (String[] args)
	{
		new ArrayTest().run();
	}

	public void run ()
	{
		BinarySearchTreeArray<String> StringsTree = new BinarySearchTreeArray<String> ();
		BinarySearchTreeArray<Integer> IntegerTree = new BinarySearchTreeArray<Integer> ();

		StringsTree.add("dog");
		StringsTree.add("turtel");
		StringsTree.add("cat");
		StringsTree.add("ferret");
		StringsTree.add("shark");
		StringsTree.add("whale");
		StringsTree.add("porpoise");

		System.out.println("First Binary Tree with strings: ");		
		StringsTree.details();		
		StringsTree.remove("ferret");
		System.out.println("First BST after Removing:  ");
		StringsTree.details();		

		IntegerTree.add(3);
		IntegerTree.add(18);
		IntegerTree.add(4);
		IntegerTree.add(99);
		IntegerTree.add(50);
		IntegerTree.add(23);
		IntegerTree.add(5);
		IntegerTree.add(101);
		IntegerTree.add(77);
		IntegerTree.add(87);


		System.out.println("Second Binary Tree with Integers: ");		
		IntegerTree.details();	
		IntegerTree.remove(50);	
		System.out.println("Second BST after Removing: ");
		IntegerTree.details();		


	}

	
	
}

