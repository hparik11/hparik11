package Lab9;

import java.util.*;

public class BinarySearchTreeArray<E> {
	
	final static int NULL = -1;
	protected ArrayList<TreeEntry<E>> tree_ary;
	protected int root = NULL;
	protected int size = 0;

        public BinarySearchTreeArray () {
		size = 0;
		root = NULL;
      	tree_ary = new ArrayList<TreeEntry<E>> ();
        }
        
	protected static class TreeEntry<E>
	{
		protected E element;
		protected int right = NULL;
		protected int left = NULL;
		
		protected int parent;

		public TreeEntry()
		{
		}

		public TreeEntry (E element, int parent)
		{
			this.element = element;
			this.parent = parent;
			this.left = NULL;
			this.right = NULL;
		}

	}

	private boolean addElement(E element, int rootIdx)
	{
		TreeEntry root = tree_ary.get(rootIdx);
		int comp = ((Comparable)element).compareTo(root.element);
		if(comp == 0)
		{
			return false;
		}
		if(comp < 0)
		{
			if(root.left != NULL)
				return addElement(element, root.left);
			else
			{
				root.left = size;
				tree_ary.set(rootIdx, root);
				TreeEntry<E> temp = new TreeEntry<E> (element, rootIdx);
				tree_ary.add(root.left, temp);
				size++;
				return true;
			}
		}
		else if(comp > 0)
		{
			if(root.right != NULL)
				return addElement(element, root.right);
			else
			{
				root.right= size;
				tree_ary.set(rootIdx, root);
				TreeEntry<E> temp = new TreeEntry<E> (element, rootIdx);
				tree_ary.add(root.right, temp);
				size++;
				return true;
			}
		}
		return false;

	}

	public boolean add(E element)
	{
		if (root == NULL)
		{
			if(element == null)
			{
				System.out.println("Exception");
				throw new NullPointerException();
			}
			root = 0;
			TreeEntry<E> temp = new TreeEntry<E> (element, NULL);
			tree_ary.add(root, temp);
			size++;
			return true;
		}else
		{
			addElement(element, root);
		}
		return true;

	}

	public boolean contains (E element)
	{
		int i = 0;

		TreeEntry temp = tree_ary.get(0);
		while(i++ < size)
		{
			int comp = ((Comparable)element).compareTo(temp.element);
			if(comp ==0)
			{
				return true;
			} else if( comp < 0)
			{
				temp = tree_ary.get(temp.left);
			}else if(comp > 0)
			{
				temp = tree_ary.get(temp.right);
			}

		}
		return false;
	}
	public int size()
	{
		return size;
	}
	public boolean remove(E element)
	{
		if (element == null)
			throw new NullPointerException();
		int i = 0;
		int tempIndex = NULL;
		TreeEntry temp = tree_ary.get(root);
		tempIndex = root;
		while(i++ < size)
		{
			int comp = ((Comparable)element).compareTo(temp.element);
		        if(comp == 0){
				break;
			}
			else if(comp < 0) {
				tempIndex = temp.left;
				temp = tree_ary.get(temp.left);
			}
			else if(comp > 0) {
				tempIndex = temp.right;
				temp = tree_ary.get(temp.right);
			}

		}
		if (tempIndex == NULL)
			return false;
		else
		{
			if(temp.left != NULL && temp.right != NULL) {
				TreeEntry removeNode = temp;
				int removeNodeIdx = tempIndex;
				tempIndex = temp.right;
				temp = tree_ary.get(temp.right);
				while(temp.left != NULL)
				{
					tempIndex = temp.left;
					temp = tree_ary.get(temp.left);
				}
				removeNode.element = temp.element;
				tree_ary.set(removeNodeIdx, removeNode);
			}
			
		
			if((temp.left == NULL) && (temp.right == NULL))
			{
				System.out.println("remove: Leaf Node");
				TreeEntry parent = tree_ary.get(temp.parent);
				if(parent.left == tempIndex) {
				
					parent.left = NULL;
					tree_ary.set(temp.parent, parent);
					return true;
				}else if(parent.right == tempIndex) {
					parent.right = NULL;
					tree_ary.set(temp.parent, parent);
					return true;
				}else {
					return false;
				}
			}else
			{	
				int replaceNodeIdx = NULL;
				if(temp.left != NULL)
					replaceNodeIdx = temp.left;
				else 
					replaceNodeIdx = temp.right;
				TreeEntry replaceNode = tree_ary.get(replaceNodeIdx);

				replaceNode.parent = temp.parent;
				if(replaceNode.parent == NULL)
				{
					replaceNodeIdx = root;
				}else if(tempIndex == replaceNode.left)
				{
					replaceNode.left = replaceNodeIdx;

				}else if(tempIndex == replaceNode.right)
				{
					replaceNode.right = replaceNodeIdx;
				}
				tree_ary.set(tempIndex, replaceNode);
				
				for(int ii = (tempIndex+1); ii < size; ii++)
				{
					TreeEntry node = tree_ary.get(ii);
					TreeEntry nodeParent = tree_ary.get(node.parent);
					if(nodeParent.left == ii)
						nodeParent.left = ii - 1;
					else if(nodeParent.right == i)
						nodeParent.right = ii - 1;
					tree_ary.set(node.parent, nodeParent);
					tree_ary.set(ii - 1, node);
				}
				
			}

		}
		size--;
		System.out.println("Remove: " +  element);
			return false;
	}

	

	public void details()
	{
		System.out.println("\tSize of Tree :" + size);
		System.out.println();
		System.out.println("\tIndex | Element  |  parent  |  left |  right |" );

		for(int i =0 ;i< size; i++)
		{
			TreeEntry temp = tree_ary.get(i);
			System.out.print("\t"+i+". ");
			System.out.print("\t"+temp.element);
			System.out.print("\t\t"+temp.parent);
			System.out.print("\t"+temp.left);
			System.out.println("\t"+temp.right);
		}
		System.out.println();
		System.out.println();
	}


	

}
