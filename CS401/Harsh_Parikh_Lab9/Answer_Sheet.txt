Output:-


Please enter the number of elements to be inserted for the first trial: 1000

The tree size is 1000
Average Height for 1000 elements 21.0
The ratio of the average height to the log, base 2, of the tree size is 2.0

The tree size is 2000
Average Height for 2000 elements 23.95
The ratio of the average height to the log, base 2, of the tree size is 2.0

The tree size is 4000
Average Height for 4000 elements 26.15
The ratio of the average height to the log, base 2, of the tree size is 2.0

The tree size is 8000
Average Height for 8000 elements 29.3
The ratio of the average height to the log, base 2, of the tree size is 2.0

The tree size is 16000
Average Height for 16000 elements 32.05
The ratio of the average height to the log, base 2, of the tree size is 2.0





--I believe that the ratio will be always 1 but as we have random values inside the tree, the height can be greater than
log(n) and that's why the ratio is grater than 1 most of the time. Note here that it always be around 2. so we van say height is approx 2log(n).

--No discrepancy.


