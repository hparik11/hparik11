package Lab9;

public class BinaryTest
{
  public static void main (String[ ] args) throws Exception
  {
    new BinaryTest().run();
  } // method main
  
  public void run() throws Exception
  {
        
    
    BinaryTreeExample<Integer> tree1 = new BinaryTreeExample<Integer>();
    tree1.add(14);
    tree1.add(8);
    tree1.add(12);
    tree1.add(10);
    tree1.add(31);
    tree1.add(40);
    tree1.add(18);
    tree1.add(30);
    tree1.add(2);
    tree1.add(20);
    
    System.out.println("\nRecursive inorder");
    tree1.inorder();
    System.out.println();
    System.out.println("\nItterative inorder");
    tree1.inorder_itr();
    
    
    
  } // method run
  
} // class BinarySearchTreeExample
