import java.io.*;
public class Pascal {

	public static void main(String args[]) throws IOException
	{
		int i,j,n;
		
		
		System.out.println("Enter number of rows to be added n=");
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		n=Integer.parseInt(br.readLine());
		int arr[][]=new int[1000][1000];                  //Initializing 2-D Array
	    
	   
		for(i=0;i<n+1;i++)
			{
			    for(j=0;j<i;j++)
			    {
			    	if(i==j || j==0)                      //This loop is only fired when row=column or 1st column is there.
			    	{
			    		arr[i][j]=1;
			    		System.out.print(arr[i][j]+"\t"); 		
			    	}
			    	else if(i>0)                              //This loop will work for other possibilities. 
			    	{
			    		arr[i][j]=arr[i-1][j-1]+arr[i-1][j];
			    		System.out.print(arr[i][j]+"\t");
			    	
			    	}
			    }
			  System.out.println();
			    	
			}
		new Pascal().Recursive();
		
			    
	}	
	
	public void Recursive() throws IOException
	{
		final String MESSAGE_1 = "The elapsed time was ";

	     final double NANO_FACTOR = 1000000000.0;  // nanoseconds per second

	     final String MESSAGE_2 = " seconds.";

	     long startTime,
	          finishTime,
	          elapsedTime;
	     
		while(true)
		{
			int R,C;
			System.out.println("\nEnter Rows= ");
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			R=Integer.parseInt(br.readLine());
			System.out.println("\nEnter Columns= ");
			BufferedReader bs=new BufferedReader(new InputStreamReader(System.in));
			C=Integer.parseInt(bs.readLine());
			startTime = System.nanoTime();
			
			
			int element;
			element=Pascal(R,C);
			
			if(element==-1)
			{
				String errormessage="Please enter rows greater than columns and Put R,C greater than zero";
				System.out.println("\n"+errormessage);
			}
			else
			System.out.println("\nPascal(R,C)= "+element); //Calling Pascal Method.
			
			
			
			finishTime = System.nanoTime();
			elapsedTime = finishTime - startTime;
			System.out.println("\n");
			System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);
		}
	}
	
	public static int Pascal(int R,int C)
	{
		
	
		if(R==C && R!=0 || C==1 )
			return 1;
	   	else if(R<C ||R==0 ||C==0 )
	   		return -1;
		else
		    return (Pascal(R-1,C-1)+Pascal(R-1,C));                      //Recursion is going on. 
	}
}
	

