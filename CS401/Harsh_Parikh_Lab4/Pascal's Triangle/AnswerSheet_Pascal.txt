1) What is the runtime complexity of the recursive version of Pascal's triangle? Estimate it from
executing the program for successively larger number of rows.

The run time complexity of pascal's triangle is O(2^n). 

Output:-

>Enter Rows= 10

Enter Columns= 5

Pascal(R,C)= 252


The elapsed time was 2.42776E-4 seconds.


>Enter Rows= 25

Enter Columns= =10

Pascal(R,C)= 3268760


The elapsed time was 0.012835149 seconds.