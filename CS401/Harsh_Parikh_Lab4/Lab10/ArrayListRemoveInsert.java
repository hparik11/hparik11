import java.util.*;

public class ArrayListRemoveInsert
{  
    public static void main (String[] args)
    {
        new ArrayListRemoveInsert().run();
    } // method main
    
    public void run()
    {
    	
    }    
	public static ArrayList<Integer> removeInt(ArrayList<Integer> randInts, Integer n)
	{
		
	int removalCount = 0;
    	while (randInts.remove(n))                         //In this loop, if we find the element to be //removed then we are incrementing the removalcount variable.
    	removalCount++;
    	if (removalCount == 0)
    		System.out.println (n + " was not found, so not removed.\n\n");
    	else if (removalCount == 1)
    		System.out.println ("The only instance of " + n +	
    				" was removed.\n\n");
    	else
    		System.out.println ("All " + removalCount + " instances of " + 
    	    				       n + " were removed.\n");   
    	
   	   	return randInts;
	}
    // Your declaration and definition of the ArrayListRemoveInsert's insert method goes here:
	public static ArrayList<Integer>  InsertAfterInt (ArrayList<Integer> randInts, 
			                                                 Integer n, Integer removeInt)
	{
		System.out.println(removeInt+" is inserted after "+n+".");
		for (int i=0;i<randInts.size();i++)
	   	 {
	   		if(randInts.get(i)==n)                 //Here in this loop, we are finding the //position of element after which we need to insert removed element. 
	   		randInts.add(i+1, removeInt);
 	   	 }
  
	return randInts;
	
    }//InsertAfterInt ends
	
}//Class ArrayListRemoveInsert
