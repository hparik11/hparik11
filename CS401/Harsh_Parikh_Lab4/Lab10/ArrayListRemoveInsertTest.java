

import java.util.ArrayList;

import junit.framework.TestCase;


public class ArrayListRemoveInsertTest extends TestCase{
	
	
	public void test(){
		
		
		ArrayList<Integer> sample_array = new ArrayList<Integer>();            //Sample Array 
		sample_array.add(2);
		sample_array.add(5);
		sample_array.add(4);
		sample_array.add(2);
		sample_array.add(4);
		sample_array.add(2);
		
		ArrayList<Integer> result1_expected = new ArrayList<Integer>();        //removal of 4 from input array
		result1_expected.add(2);
		result1_expected.add(5);
		result1_expected.add(2);
		result1_expected.add(2);
		
		ArrayList<Integer> result2_expected = new ArrayList<Integer>();        //insertion of 4 after each 2
		result2_expected.add(2);
		result2_expected.add(4);
		result2_expected.add(5);
		result2_expected.add(2);
		result2_expected.add(4);
		result2_expected.add(2);
		result2_expected.add(4);
		
		
		ArrayListRemoveInsert.removeInt(sample_array,4);
		assertEquals(result1_expected, sample_array);                  //Comparing two Array lists using assertEquals
		ArrayListRemoveInsert.InsertAfterInt(sample_array, 2, 4);
		assertEquals(result2_expected, sample_array);
			
	}
	
}