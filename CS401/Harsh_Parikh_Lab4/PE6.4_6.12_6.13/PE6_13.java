import java.util.ArrayList;


public class PE6_13<T> {
	
	public static <T> ArrayList <T> uniquefy (ArrayList <T> list)
	{
		
		ArrayList<T> resultList = new ArrayList<T>(list.size());    //Initializing resultList arraylist. 
		for(T obj: list)
		{
			if(resultList.contains(obj)==false)                     //Comparing objects of myList with resultList and if object is
																	// not part of myList then it will copy that into resultList.
			resultList.add(obj);
		}
		System.out.println();
		System.out.println("After uniquefy function,My Array");
		System.out.println(resultList);
		
		return list;
	}
	

	
	public static void main(String args[])
	{
		ArrayList<Object> myList = new ArrayList<Object>();
		
		myList.add(3);
		myList.add(8);                    //adding elements into myList
		myList.add(6);
		myList.add(4);
		myList.add(7);
		myList.add(8);
		myList.add(9);
		myList.add(4);
		System.out.println("My Array");
		System.out.print(myList);
		System.out.println();
		uniquefy(myList);                 // calling uniquefy method.
	}
	
}
