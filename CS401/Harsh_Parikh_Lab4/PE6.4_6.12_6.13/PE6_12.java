import java.util.ArrayList;
import java.util.Iterator;


public class PE6_12 {
public static void main(String args[])
{
	ArrayList<String> words = new ArrayList<String>();       //Initializing words Arraylist.
	words.add("Harsh");
	words.add("Jay");
	words.add("Pari");
	words.add("Hars");
	words.add("Road");
	
	
	
	new PE6_12().usingindex(words);
	System.out.println("\n");
	
	new PE6_12().usingiterator(words);
	
	System.out.println("\n");
	
	new PE6_12().usingFor(words);
	
	
}	
	public void usingindex(ArrayList<String> words)            // Using Index Method 
	{
		System.out.println("Index version");
		for(int i=0;i<words.size();i++)
		{
			if(((String) words.get(i)).length()==4)
			{
				System.out.println(words.get(i));
			}
		
		}
    }
	public void usingiterator(ArrayList<String> words)         //Using Iterator Method
	{
		System.out.println("Explicit Iterator");
		
		Iterator<String> itr = words.iterator();
		String word;
		while (itr.hasNext ())
		{
		word = itr.next();
		if (word.length()==4)
		System.out.println (word);
		} // while
	}
	public void usingFor(ArrayList<String> words)               //Using Enhanced For Method
	{
		System.out.println("Enhanced For");
		for (String word :  words)
			if (word.length()==4)
			System.out.println (word);
	}
	
	
}
