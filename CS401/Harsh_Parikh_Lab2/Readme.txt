Part 1:-

Exercise 2.11:-

Here, my main class is Excercise2_11.java and I have put constructors and methods into Company.java file.


1)First, I have taken one variable X and then I have compared it with itself using equals method. So it should be true. Hence, Reflexivity Proved. 
2)Then, I have taken two different variables X,Y and given different values to them and compared with each other. So they should be false. Hence, Symmetry Proved. 
3)Then for Transitivity, I have taken three different variables X,Y and Z. I put the parameters of three variables same. So they should return true.
4)Then for Consistency, I have taken two variables and assigned same values to them. so they should return true.
5)At the last, I am Comapring one variable with some values with null values. So it should give false value. So Actuality proved. 


Excercise 2.12:-

Here, my main class is Excercise2_12.java and make one JUnit Test class named Excercise2_12Test.java.

1)I have put constructors and equals method in main class.
2)In Test file, I have made 4 methods to test the equality.
3)In test(), I am comparing two different variables with different values. So they should return false after comparision.
4)In test1(), I am comparing putting one parameter same and other different for both variables and it also returns false.
5)In test2(), I do the same thing as done in test1(), but I change the value of different parameter.
6)In test3(), I am comparing two different variables with same values and they should return the true. 



So this way, 2.11 and 2.12 are running. 
