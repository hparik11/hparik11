import java.text.DecimalFormat;

public class Excercise2_12
{
	public String name;
	public double EmplID;
	final static DecimalFormat MONEY = new DecimalFormat (" $0.00");
	 

	public Excercise2_12(String name, double EmplID)
	{
		this.name = name;
		this.EmplID = EmplID;
	} 

	
	public boolean equals (Object obj)
	{
		if (!(obj instanceof Excercise2_12))
			return false;
		Excercise2_12 full = (Excercise2_12)obj;
		return name.equals (full.name) && MONEY.format (EmplID).equals (MONEY.format (full.EmplID));
	}
} 
