import static org.junit.Assert.*;

import org.junit.Test;


public class Excercise2_12Test {

	@Test
	public void test() {
		System.out.println("Returns false if both parameters are different. ") ;
		Excercise2_12 X=new Excercise2_12("Harsh",11);
		Excercise2_12 Y=new Excercise2_12("Parikh",20);
		
		assertFalse(X.equals(Y));
		
		System.out.println("X.equals(Y) is "+X.equals(Y));
	}
	
	@Test
	public void test1() {
		System.out.println("Returns false if one parameter(Integer) is different. ") ;
		Excercise2_12 X=new Excercise2_12("Harsh",11);
		Excercise2_12 Y=new Excercise2_12("Harsh",20);
		
		assertFalse(X.equals(Y));
		
		System.out.println("X.equals(Y) is "+X.equals(Y));
	}
	
	@Test
	public void test2() {
		System.out.println("Returns false if one parameter(String) is different. ") ;
		Excercise2_12 X=new Excercise2_12("Harsh",11);
		Excercise2_12 Y=new Excercise2_12("Parikh",11);
		
		assertFalse(X.equals(Y));
		
		System.out.println("X.equals(Y) is "+X.equals(Y));
	}
	
	@Test
	public void test3() {
		System.out.println("Returns true only if both parameters are same ") ;
		Excercise2_12 X=new Excercise2_12("Harsh",11);
		Excercise2_12 Y=new Excercise2_12("Harsh",11);
		
		assertTrue(X.equals(Y));
		
		System.out.println("X.equals(Y) is "+X.equals(Y));
	}
}
