import java.text.DecimalFormat;

public class Company 
{
	public String name;
	public double EmplID;
	final static DecimalFormat MONEY = new DecimalFormat (" $0.00");
	Company()
	{
		final String EMPTY_STRING = "";
		name = EMPTY_STRING;
		EmplID = 0.00;
	} 

	public Company(String name, double EmplID)
	{
		this.name = name;
		this.EmplID = EmplID;
	} 

	public boolean equals (Object obj)
	{
		if (!(obj instanceof Company))
			return false;
		Company full = (Company)obj;
		return name.equals (full.name) && MONEY.format (EmplID).equals (MONEY.format (full.EmplID));
	}
} 
