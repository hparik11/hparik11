public class Excercise2_11 extends Company
{
	public static void main(String args[])
	{
		
		//Reflexivity 
		Company x=new Company("Harsh",100.00);
		System.out.println("x.equals(x) is "+ x.equals(x));
		System.out.println("'Reflexivity Proved'");
		System.out.println();
		
		
		//Symmetry
		Company y=new Company("Ashwin",200);
		System.out.println("x.equals(y) is "+ x.equals(y));
		System.out.println("y.equals(x) is "+ y.equals(x));
		System.out.println("'Symmetry Proved'");
		System.out.println();
		
		
		//Transitivity
		Company a=new Company("Raj",100);
		Company b=new Company("Raj",100);
		Company c=new Company("Raj",100);
		System.out.println("a.equals(b) is "+ a.equals(b));
		System.out.println("b.equals(c) is "+ b.equals(c));
		System.out.println("c.equals(a) is "+ c.equals(a));
		System.out.println("'Transitivity Proved'");
		System.out.println();
		
		
		//Consistency
		Company e=new Company("Yaminee",101);
		Company f=new Company("Yaminee",101);
		System.out.println("e.equals(f) is "+ e.equals(f));
		System.out.println("e.equals(f) is "+ e.equals(f));
		System.out.println("e.equals(f) is "+ e.equals(f));
		System.out.println("e.equals(f) is "+ e.equals(f));
		System.out.println("'Consistency Proved'");
		System.out.println();
		
		
		//Actuality
		Company d=new Company("Harsh",100.00);
		System.out.println("d.equals(null) is  "+ d.equals(null));
		System.out.println("'Actuality Proved'");
		System.out.println();
		
		
		}
}


