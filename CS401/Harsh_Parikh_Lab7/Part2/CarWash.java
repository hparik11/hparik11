package Lab7;

import java.util.*;

public class CarWash
{
     public final static String Overflow = " (Overflow)\n";
     
     public final static String HEADING = "\n\nTime\tEvent\t\tWaiting Time\n";
     public final static int MAX_SIZE = 5; 
     protected Queue<Car> carQueue;  
     protected LinkedList<String> results;  
     protected int  meanServiceTime,
		           	nextDepartureTime,
		           	maxArrivalTime,
		           	meanArrivalTime,
		           	nextArrivalTime,
		           	currentTime,
		           	sumOfWaitingTimes,   
		           	overflowCount,
		           	numberOfCars,
                    waitingTime,
                    totalArrivedCars;

     protected Random random;
     protected static int RANDOM_SEED = 100;

     public CarWash(int meanArrivalTime, int meanServiceTime, int maxArrivalTime)
     {
         carQueue = new LinkedList<Car>();
         results = new LinkedList<String>();
         random = new Random(RANDOM_SEED);
         results.add (HEADING);
         this.meanArrivalTime = meanArrivalTime;
         this.meanServiceTime = meanServiceTime;
         this.maxArrivalTime = maxArrivalTime;
         this.nextArrivalTime = getArrivalTime();
         currentTime = 0;
         numberOfCars = 0;
         waitingTime = 0;
         sumOfWaitingTimes = 0;
         nextDepartureTime = maxArrivalTime;  
     } 
     
     public void Process()
     {
    	 while(nextArrivalTime < maxArrivalTime)
    	 
    	 {
			totalArrivedCars=totalArrivedCars+1;
			NextCar(nextArrivalTime);
			nextArrivalTime = getArrivalTime();
    	 }
    	 finish();
     }
     
     protected int getServiceTime()
     {
    	 double rand = random.nextDouble();
    	 return (int)Math.round ((-meanServiceTime*Math.log (1 - rand)));
     }
     
     protected int getArrivalTime()
     {
           double rand = random.nextDouble();
           return nextArrivalTime + (int)Math.round ((-meanArrivalTime*Math.log (1 - rand)));
     } 
     
     
     public LinkedList<String> NextCar (int nextArrivalTime)
     {
         final String BAD_TIME =
             "The time of the next arrival cannot be less than the current time.";
         if (nextArrivalTime < currentTime)
              throw new IllegalArgumentException (BAD_TIME);
         while (nextArrivalTime >= nextDepartureTime)
              Departure();
         return Arrival (nextArrivalTime);
     } 

     protected LinkedList<String> Arrival (int nextArrivalTime)
     {        
         final String ARRIVAL = "\tArrival";

         currentTime = nextArrivalTime;         
         if (carQueue.size() == MAX_SIZE)        
         { 
             results.add (Integer.toString (currentTime) + ARRIVAL + Overflow);
             overflowCount++;
         }
         else
         {
              results.add (Integer.toString (currentTime) + ARRIVAL);
              numberOfCars++;
              if (nextDepartureTime == maxArrivalTime)  // if no car is being washed
                  nextDepartureTime = currentTime + getServiceTime();
              else
                  carQueue.add (new Car (nextArrivalTime));
              results.add ("\n");
         } 
         return results;
     } 


     protected LinkedList<String> Departure()
     {
         final String DEPARTURE = "\tDeparture\t\t";
         int arrivalTime;
         currentTime = nextDepartureTime;
         results.add (Integer.toString (currentTime) + DEPARTURE +
                         Integer.toString (waitingTime) + "\n");
         if (!carQueue.isEmpty())
         {
              Car car = carQueue.remove();
              arrivalTime = car.getArrivalTime();
              waitingTime = currentTime - arrivalTime;
              sumOfWaitingTimes += waitingTime;
              nextDepartureTime = currentTime + getServiceTime();
     
         } 
         else
         {
              waitingTime = 0;
              nextDepartureTime = maxArrivalTime; 
         } 
         return results;
     } 

     public LinkedList<String> finish()
     {
        
         while (!carQueue.isEmpty())  
               Departure();
         return results;
     } 


     public LinkedList<String> Results()
     {

         final String AVG_QUEUE =
        		 "\nAverage queue length is ";
         final String AVG_WAIT_TIME =
        		 "\nAverage waiting time is ";
         final String CAR_NULL = 
        		 "There were no cars in the car wash.\n";
         final String OVERFLOW =
        		 "\nThe number of overflows was ";
         if (numberOfCars == 0)
              results.add (CAR_NULL);
         else
         {
             	results.add (AVG_WAIT_TIME + Double.toString (
                          Math.round(( (double) sumOfWaitingTimes / numberOfCars) * 10 ) / 10.0) + " minutes per car.");
             	results.add (AVG_QUEUE + Double.toString (
                          Math.round(( (double) sumOfWaitingTimes / nextDepartureTime) * 10 ) / 10.0) + " cars per minute.");
             	results.add(OVERFLOW + overflowCount + ".");
         }
         return results;
     } 

} 