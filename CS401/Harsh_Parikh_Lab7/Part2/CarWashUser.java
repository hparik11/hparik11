package Lab7;

import java.util.*;

public class CarWashUser
{
    public static void main (String[] args)
    {
        new CarWashUser().run();
    }
     
    public void run()
    {         
    	final String INPUT_PROMPT_1 = "\nPlease Enter mean Arrival Time: ";
    	final String INPUT_PROMPT_2 = "\nPlease Enter mean Service Time: ";
    	final String INPUT_PROMPT_3 = "\nPlease Enter maximum Arrival Time: ";
    	final String ERROR_MESS  = "\n ERROR!! Wrong Input ";
    	@SuppressWarnings("resource")
		Scanner sc = new Scanner (System.in);    
        int meanArrivalTime;
        int meanServiceTime;
        int maxArrivalTime;

        while (true)
        {
        	try
        	{
        		System.out.print (INPUT_PROMPT_1);
        		meanArrivalTime = sc.nextInt(); 
        		System.out.print (INPUT_PROMPT_2);
        		meanServiceTime = sc.nextInt();
        		System.out.print (INPUT_PROMPT_3);
        		maxArrivalTime = sc.nextInt(); 
        		
        		int ret = checkInput(meanArrivalTime, meanServiceTime, maxArrivalTime);
        		if(ret == 0)
        		{
        			CarWash carWash = new CarWash(meanArrivalTime, meanServiceTime, maxArrivalTime);
        			carWash.Process();
        			printResults(carWash);
        		}
        		else
        		{
        			System.out.print(ERROR_MESS);
        		}

        	}
	        catch (Exception e)
			{
			System.out.println(e);
			}
		}      
    }


    public int checkInput(int arrivalTime, int serviceTime, int maxArrivalTime)
    {
    	return 0;
    }

    public void printResults (CarWash carWash)
    {
        LinkedList<String> results = carWash.Results();
        for (String s : results)
            System.out.print (s);
    } 

} 
