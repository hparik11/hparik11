package Lab7;

public class CS401QueueLinkedListImpl<E> implements CS401QueueInterface<E>  {
	   private LinkEntry<E> head;
	   private LinkEntry<E> tail;
	   private int num_elements=0;

	   public void add(E element) {

	      /*** Add code ***/
		   LinkEntry<E> ne = new LinkEntry<E>();
		      ne.element = element;

		      if (head == null && tail == null)
		      {
		          head = tail = ne;
		          
		      }
		      else
		      {
		         tail.next = ne;
		         tail=ne;
		         
		      }
		      num_elements+=1;      
		    
		      
	   }//Add Method

	   public E remove()  {
	      
	      /*** Add code ***/
		   E ne=head.element;
		   head=head.next;
		   num_elements-=1;
		   return ne;   

	   }//Remove Method
	   
	   public E peek()  {

	      /*** Add code ***/
		   E e;
		   e=head.element;
		   return e;

	   }

	   public boolean is_empty()  {

	      /*** Add code ***/
		   if(num_elements==0)
		   {
			   return true;
		   }
		   else
			   return false;
			   

	   }

	   public boolean is_full()  {

	      /*** Add code ***/
		   
		   return false;

	   }
	   public int size()
	   {
		   return num_elements;
	   }

	   /* ------------------------------------------------------------------- */
	   /* Inner classes                                                      */
	   protected class LinkEntry<E>
	   {
	      protected E element;
	      protected LinkEntry<E> next;

	      protected LinkEntry() { element = null; next = null; }
	   }
	   
	   
	   public static void main(String args[])
	   {
		   CS401QueueLinkedListImpl<String> obj=new CS401QueueLinkedListImpl<String>();
		   obj.add("my");
		   obj.add("name");
		   obj.add("is");
		   obj.add("Harsh");
		   
		   String a=obj.remove();
		   System.out.println(a);
		   String b=obj.remove();
		   System.out.println(b);
		   String c=obj.remove();
		   System.out.println(c);
		   String d=obj.remove();
		   System.out.println(d);
		 
		  // System.out.println(obj.size());
		   System.out.println("Empty ? " +obj.is_empty());       // After removing all elements, It should return true. 
		   
	   }//MAin Method
	}
