package Lab7;


	public class CS401QueueArrayFixedImpl<E> implements CS401QueueInterface<E>  {
	   private E[] data;
	   private int front, back;
	   private static int capacity;

	   public CS401QueueArrayFixedImpl(int num_elems)   {
	      capacity = num_elems;
	      data = (E[]) new Object[capacity];
	      front = back = 0;    
	   }//Initialization

	   public void add(E element) {

	      /*** Add code ***/
		   if(back<capacity)
		   {
			   data[back]=element;
			   back++;
		   }
		   else
			   throw new IndexOutOfBoundsException();

	   }//Add Method

	   public E remove() 
	   {
	      
	      /*** Add code ***/
		   
		   if(back!=0)
		   {
			   E element;
			   element=data[front];
			   
			   front++;
			   return element;
			  		   
			}
		   else
			   throw new IndexOutOfBoundsException();
		
		   
	   }//Remove Method
	   

	   public E peek()  {

	      /*** Add code ***/
		   E e=data[front];
		   return e;
	   }

	   public boolean is_empty()  {

	      /*** Add code ***/
		   if(front==back)
		   {
			   return true;
		   }
		   else
			   return false;
		   
	   }

	   public boolean is_full()  {

	      /*** Add code ***/
		   if(back==capacity)
			   return true;
		   else
			   return false;
	   }
	   
	   
	   
	   public static void main(String args[])
	   {
		   CS401QueueArrayFixedImpl<String> obj=new CS401QueueArrayFixedImpl<String>(5);
		   obj.add("This");
		   obj.add("is");
		   obj.add("my");
		   obj.add("Lab-7");
		   obj.add("Assignment");
		   
		   System.out.println("First Element: "+ obj.peek());            //First Element- Front points at
		   System.out.println("Full ? "+obj.is_full());
		   String a=obj.remove();
		   System.out.println(a);
		   String b=obj.remove();
		   System.out.println(b);
		   String c=obj.remove();
		   System.out.println(c);
		   String d=obj.remove();
		   System.out.println(d);
		   String e=obj.remove();
		   System.out.println(e);
		   
		   System.out.println("Empty ? " +obj.is_empty());       // After removing all elements, It should return true. 
		   
		   
		   
	   }//Main Method
	   
	}

	
