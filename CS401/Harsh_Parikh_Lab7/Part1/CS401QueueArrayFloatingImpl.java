package Lab7;

/* 
 * Floating front and back implementation of a queue using an array */

public class CS401QueueArrayFloatingImpl<E> implements CS401QueueInterface<E>  {
   private E[] data;
   private int front, back;
   private static int capacity;

   public CS401QueueArrayFloatingImpl(int num_elems)   {
      capacity = num_elems;
      data = (E[]) new Object[capacity];
      front = back = 0;    
   }

   public void add(E element) {

      /*** Add code ***/
	   
	   if (back<capacity)
	   {
			 data[back]=element;
			 back=(back+1)%capacity;
			 
	   }
	   else
	   {
			 throw new IndexOutOfBoundsException();
	   }
	      
   }//Add Method

   public E remove()  {
      
      /*** Add code ***/
	   if((front-back)<capacity && ((front-back)!=0 || 
		   		(data[front]!=null 
		   		&& data[back]!=null)))
	   {
		   E element;
		   element=data[front];
		   front=(front+1)%capacity;
		   
		   
		   return element;
		   
	   }
	   else
		   throw new IndexOutOfBoundsException();
	   
   }//Remove Method
   
   public E peek()  {

      /*** Add code ***/
	   E e=data[front];
	   return e;
   }

   public boolean is_empty()  {

      /*** Add code ***/
	   if(front==back)
	   {
		   return true;
	   }
	   else
		   return false;

   }

   public boolean is_full()  {

      /*** Add code ***/
	   if((front-back)==0 && data[front]!=null)
		   return true;
	   else
		   return false;

   }
   
   public static void main(String args[])
   { 
   CS401QueueArrayFloatingImpl<Character> obj=new CS401QueueArrayFloatingImpl<Character>(5);
  
   	obj.add('a');	
    obj.add('b');
    obj.add('c');
    obj.add('d');
    obj.add('e');
    
   
   System.out.println("First Element: "+ obj.peek());            //First Element- Front points at
   System.out.println("Full ? "+obj.is_full());
   char a=obj.remove();
   System.out.println(a);
   char b=obj.remove();
   System.out.println(b);
   char c=obj.remove();
   System.out.println(c);
   char d=obj.remove();
   System.out.println(d);
   char e=obj.remove();
   System.out.println(e);
//   
   System.out.println("Empty ? " +obj.is_empty());       // After removing all elements, It should return true. 
   
   }//Main
}
