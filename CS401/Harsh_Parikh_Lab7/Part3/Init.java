package Lab7;



public class Init {
	private String name;
	private int value;
	
	public Init(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public boolean equals(Object obj) {
		Init ne = (Init) obj;
		return this.name.equals(ne.name);
	}
	
}

