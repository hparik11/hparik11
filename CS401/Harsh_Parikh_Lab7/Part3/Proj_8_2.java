package Lab7;

import java.util.*;

public class Proj_8_2 {
	
	public static void main(String[] args) {
		new Proj_8_2().run();
	}
	
	final String SEPERATOR = "*/%+->=<!&|()";
	
	public void run() {
	
		Scanner sc = new Scanner(System.in);
		String condtion;
		
		final String SENTINEL = "$";
	    final String INPUT_PROMPT = "Please enter a condition (or " + SENTINEL + " to quit):";
	    
	    final String OUTPUT_PROMPT_1 = "\nThe value of the condition is ";
	    		
	    while (true)
	    {
	    	try
	    	{
	    		System.out.print(INPUT_PROMPT);
		    	condtion = sc.nextLine();
		    	if (condtion.equals(SENTINEL)) {
		    		break;
		    	}
		    	infix2Postfix(condtion);
		    	
		    	for (Init var : variables) {
					var.setValue(read(sc, var));
				}
		    	
		    	boolean result = evaluate();
		    	
		    	System.out.print(OUTPUT_PROMPT_1 + result);
		    	System.out.print("\n\n");
		    	
		    	clear();
		    	
	    	} 
	    	catch (Exception e) 
	    	{
	    		System.out.println(e.getMessage());
	    		sc.nextLine();
	    	}
	    }
	}
	
	private void clear()
	{
		stack = new CS401StackLinkedListImpl<>();
		postfix_qu = new LinkedList<>();
		op_factory = new OperatorFactory();
		variables = new ArrayList<>();
	}
	
	
	int read(Scanner sc ,Init value) {
		int d;
		while (true) {
			try {
				System.out.print("Please enter a value of the varible '" + value.getName() + "':");
				d = sc.nextInt();
				sc.nextLine();
				break;
			} catch (Exception e) {
				System.out.println("please input again!");
				sc.nextLine();
			}
		}
		return d;
	}
	
	private CS401StackLinkedListImpl<Operator> stack;
	private Queue<String> postfix_qu;
	private OperatorFactory op_factory;
	private ArrayList<Init> variables;
	
	public Proj_8_2() {
		clear();
	}
	
	private void processOperator(String token) {
		Operator top;
		Operator op = op_factory.create(token);
		
		switch (token) {
			case "(":
				stack.push(op);
				break;
			case ")":
				while ((top = stack.pop()) != null) {
					if (!top.getName().equals("(")) {
						postfix_qu.add(top.getName());
					} else {
						break;
					}
				}
				break;
			default:
				while ((top = stack.peek()) != null) {
					if (top.getPriority() >= op.getPriority()) {
						stack.pop();
						postfix_qu.add(top.getName());
					} else {
						break;
					}
				}
				stack.push(op);
				break;
		}
	}
	
	private void Operand(String token) {
		Init var = new Init(token);
		if (!variables.contains(var)) {
			variables.add(var);
		}
		postfix_qu.add(token);
	}
	
	
	private void infix2Postfix(String infix_exp) {
		
		StringTokenizer st = new StringTokenizer(infix_exp, SEPERATOR, true);
		String token = "", nextToken = "";
		Operator top;
		while (st.hasMoreTokens()) {
			token = st.nextToken().trim();
			if (token.length() == 0) {
				continue;
			}
			switch (token) {
				case "*":
				case "/":
				case "%":
				case "+":
				case "-":
				case "(":
				case ")":
					processOperator(token);
					break;
				case ">":
					nextToken = st.nextToken().trim();
					if (nextToken.equals("=")) {
						processOperator(">=");
					} else {
						processOperator(">");
						if (!nextToken.equals("")) {
							Operand(nextToken);
						}
					}
					break;
				case "<":
					nextToken = st.nextToken().trim();
					if (nextToken.equals("=")) {
						processOperator("<=");
					} else {
						processOperator("<");
						if (!nextToken.equals("")) {
							Operand(nextToken);
						}
					}
					break;
				case "=":
					st.nextToken();
					processOperator("==");
					break;
				case "&":
					st.nextToken();
					processOperator("&&");
					break;
				case "!":
					st.nextToken();
					processOperator("!=");
					break;
				case "|":
					st.nextToken();
					processOperator("||");
					break;
				default:
					Operand(token);
					break;
			}
		}
		while ((top = stack.pop()) != null) {
			postfix_qu.add(top.getName());
		}
	}
	
	public boolean evaluate() {
		CS401StackLinkedListImpl<Object> stack = new CS401StackLinkedListImpl<>();
		String token;
		int operand1, operand2;
		boolean b1, b2;
		
		while (!postfix_qu.isEmpty()) {
			token = postfix_qu.remove();
			switch (token) {
				case "+":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 + operand2);
					break;
				case "-":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 - operand2);
					break;
				case "*":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 * operand2);
					break;
				case "/":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 / operand2);
					break;
				case "%":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 % operand2);
					break;
				case ">":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 > operand2);
					break;
				case ">=":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 >= operand2);
					break;
				case "<=":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 <= operand2);
					break;
				case "<":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 < operand2);
					break;
				case "==":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 == operand2);
					break;
				case "!=":
					operand2 = (int)stack.pop();
					operand1 = (int)stack.pop();
					stack.push(operand1 != operand2);
					break;
				case "&&":
					b2 = (boolean)stack.pop();
					b1 = (boolean)stack.pop();
					stack.push(b1 && b2);
					break;
				case "||":
					b2 = (boolean)stack.pop();
					b1 = (boolean)stack.pop();
					stack.push(b1 || b2);
					break;
				default:
					
					stack.push(getOperandValue(token));
					break;
							
			}
		}
		return (boolean)stack.pop();
	}
	
	private int getOperandValue(String token) {
		for (Init var : variables) {
			if (var.getName().equals(token)) {
				return var.getValue();
			}
		}
		return 0;
	}
	
	protected class Operator {
		private String name;
		private int priority;
		
		public Operator(String name, int priority) {
			this.name = name;
			this.priority = priority;
		}
		
		public String getName() {
			return name;
		}
		public int getPriority() {
			return priority;
		}
		
	}
	
	protected class OperatorFactory {
		
		private Operator mul_op, div_op, mod_op;                                     //priority = 6
		private Operator add_op, sub_op;                                             //priority = 5
		private Operator greater_op, greater_equal_op, less_euqal_op, less_op;       //priority = 4
		private Operator equal_op, not_equal_op;                                     //priority = 3
		private Operator and_op;                                                     //priority = 2
		private Operator or_op;                                                      //priority = 1
		private Operator left_paren_op,right_paren_op;                               //priority = 0
		
		
		
		public Operator create(String name) {
			Operator op = null;
			int priority = 0;
			boolean bValid = true;
			switch (name) {
				case "(":
					op = left_paren_op;
					priority = 0;
					break;
				case ")":
					op = right_paren_op;
					priority = 0;
					break;
				case "||":
					op = or_op;
					priority = 1;
					break;
				case "&&":
					op = and_op;
					priority = 2;
					break;
				case "==":
					op = equal_op;
					priority = 3;
					break;
				case "!=":
					op = not_equal_op;
					priority = 3;
					break;
				case ">":
					op = greater_op;
					priority = 4;
					break;
				case ">=":
					op = greater_equal_op;
					priority = 4;
					break;
				case "<=":
					op = less_euqal_op;
					priority = 4;
					break;
				case "<":
					op = less_op;
					priority = 4;
					break;
				case "+":
					op = add_op;
					priority = 5;
					break;
				case "-":
					op = sub_op;
					priority = 5;
					break;
				case "*":
					op = mul_op;
					priority = 6;
					break;
				case "/":
					op = div_op;
					priority = 6;
					break;
				case "%":
					op = mod_op;
					priority = 7;
					break;
				default:
					bValid = false;
					break;
			}
			if (bValid)
			{
				if (op == null) 
				{
					op = new Operator(name, priority);
				}
				return op;
			} 
			else
			{
				return null;
			}
		}
	}
}

