package cs401;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author Harsh_laptop
 */

public class Lab01 {
    public static void main(String args[]) throws IOException
    {
        float m,n;
        InputStreamReader in=new InputStreamReader(System.in);
        BufferedReader br=new BufferedReader(in);
        
        System.out.println("Miles traveled :");
        m=Float.valueOf(br.readLine());
        System.out.println("Gallons of fuel consumed: ");
        n=Float.valueOf(br.readLine());
        System.out.println("Average MPG: "+(m/n));
    }
}
